#######################################################
# The first step is to load the package & set the wd: #
#######################################################
library(BMEA)
library(limma)
#library(plyr)
library(dplyr)
library(magrittr)
library(tibble)
library(reshape2)
library(ggplot2)
library(grid)
library(gtable)
library(snow)

# Define the chipType using the default Affy cdf. This is required for the BG correction steps
chipType <- "HuEx-1_0-st-v2"
affyCdf <- AffymetrixCdfFile$byChipType(chipType)

# Form the celSet using the aroma.affymetrix setup:
cs <- AffymetrixCelSet$byName("Mixture", cdf=affyCdf)

## Quantile normalisation
qn <- QuantileNormalization(cs)
csN <- process(qn, verbose=verbose)

#####################################################################################################
# Get the background Priors #
#####################################################################################################

# Fit the background parameters for the dataset
bgParam <- fitBackgroundParameters(csN, cdf=affyCdf, 
                                   bgProbes="r2.antigenomic.bgp", method="MAT")
bgBins <- defineMatBins(bgParam)

# Change the cdf if using pre-existing files & a custom cdf:
ensCdf <- AffymetrixCdfFile$byChipType(chipType, tags="U-ENSG,G-ENSE,v14.1")
setCdf(csN, ensCdf)

# Get the number of units
nUnits <- nbrOfUnits(ensCdf)

## Assign the probes to bins
bgCelSet <- assignBgPriors(celSet=csN, 
                           seqFile="HuEx-1_0-st-v2,U-ENSG,G-ENSE,v14.1.probe_tab", 
                           bgBins, bgParam, overWrite=FALSE)

############################################################
## Fit only the 100% and 50% arrays for the initial lists ##
############################################################

celNames <- R.filesets::getNames(csN)
mixLevels <- gsub(".+(mix[0-9abc]*)_.+", "\\1", celNames)
sub2Fit <- grepl("mix(1|5|9)", mixLevels)
csN.sm <- extract(csN, sub2Fit)
bgCs.sm <- bgCelSet
bgCs.sm$lambda <- extract(bgCs.sm$lambda, sub2Fit)
bgCs.sm$delta <- extract(bgCs.sm$delta, sub2Fit)

# Form the smaller conditions vector
condLevels <- c(mix1 = "Heart100", mix9 = "Brain100", mix5a = "A50.50", mix5b = "B50.50", mix5c = "C50.50")
cond.sm <- factor(condLevels[mixLevels[sub2Fit]], levels = as.character(condLevels))

# Set up the contrast matrix
contMatrix <- makeContrasts(Heart100 - Brain100, 
                            A50.50 - B50.50,
                            A50.50 - C50.50,
                            B50.50 - C50.50,
                            levels=levels(cond.sm))

# Set up the mcmcParameters:
mcmcParam <- vector("list",4)
mcmcParam[[1]] <- 3L
mcmcParam[[2]] <- 12000L
mcmcParam[[3]] <- 6000L
mcmcParam[[4]] <- 6L
names(mcmcParam) <- c("nChains","nIter","nBurnin","nThin")

# Set the verbose level
verbose <- Arguments$getVerbose(-8, timestamp=TRUE)

# Leave out titin!!!
units <- 1:nUnits
units <- units[-9732]

# Run them in parallel
cl <- makeCluster(20, type="SOCK")
parFit.sm <- fitBmea.Snow(celSet=csN.sm, bgCelSet=bgCs.sm, cl=cl, units=units, batchSize=16, conditions=cond.sm, contMatrix=contMatrix, mcmcParam=mcmcParam)
stopCluster(cl)
mergeNodes(celSet=csN.sm, as.list(names(parFit.sm$units)), paramToWrite=c("c", "mu","phi"))

# Clear out the nodes:
clearNodes(names(parFit.sm$units))

# The model Parameters
csMu <- AffymetrixCelSetList(csN, type="model", tags="mu")

# Check for not-expressed genes
noExp <- extractBmeaArray(csMu)[,"mean","Brain100"]<=0

sum(!noExp) # [1] 29394
sum(noExp) # [1] 5808
mean(noExp) # [1] 0.1649906
# So about 16% of genes are not detectable above background (+ TITIN)

##############################
## Fit Using RMA/PLM/FIRMA  ##
##############################

# Do RMAbackground correction
bcRma <- RmaBackgroundCorrection(csN.sm, tag="RMA")
csBCRma <- process(bcRma,verbose=verbose)

# Fit the model using PLM.
plmTrRma <- ExonRmaPlm(csBCRma, mergeGroups=TRUE) 
fit(plmTrRma, verbose=verbose)

# Setup the QA plots
qamTrRma <- QualityAssessmentModel(plmTrRma)

# Do the NUSE plot.
nuseRma <- plotNuse(qamTrRma)
nuseBox <- list(names = names(nuseRma),
                stats = sapply(nuseRma, FUN=function(x){x$stats}))
bxp(nuseBox, main="a) NUSE",las=2)
abline(h=1,col=1)
abline(h=1.05,lty=2,col="red")

# Do the RLE plots
rleRma <- plotRle(qamTrRma)
rleBox <- list(names = names(rleRma),
               stats = sapply(rleRma, FUN=function(x){x$stats}))
bxp(rleBox, main="b) RLE",las=2)
abline(h=0,col=1)
# All good

##########################
# ExpressionSet Creation #
##########################

cesTrRma <- getChipEffectSet(plmTrRma)
transcripts <- extractDataFrame(cesTrRma, addNames=TRUE)
eSetMix <- transcripts %>%
  dplyr::select(starts_with("TisMix")) %>%
  log2() %>%
  as.matrix() %>%
  set_rownames(transcripts$unitName) %>%
  ExpressionSet()
featureData(eSetMix) <- transcripts %>%
  set_rownames(.$unitName) %>%
  dplyr::select(-unitName, -starts_with("TisMix")) %>%
  AnnotatedDataFrame()
phenoData(eSetMix) <- data.frame(condition = cond.sm) %>%
  set_rownames(sampleNames(eSetMix)) %>%
  AnnotatedDataFrame()
rm(transcripts)

saveRDS(eSetMix, "eSetMix.RDS",compress = FALSE)


#########################
## Exon-level Analysis ##
#########################

## Get the firma scores
firma <- FirmaModel(plmTrRma) #Define the action
fit(firma, verbose=verbose) #Perform the action
fs <- getFirmaScores(firma) #Get the scores
exFirma <- extractDataFrame(fs, addNames=TRUE) 

eSetMixFirma <- exFirma %>%
  dplyr::select(starts_with("TisMix")) %>%
  log2() %>%
  as.matrix() %>%
  set_rownames(paste0(exFirma$unitName, exFirma$groupName)) %>%
  ExpressionSet()
featureData(eSetMixFirma) <- exFirma %>%
  set_rownames(paste0(.$unitName, .$groupName)) %>%
  dplyr::select(-ends_with("Name"), -starts_with("TisMix")) %>%
  AnnotatedDataFrame()
phenoData(eSetMixFirma) <- phenoData(eSetMix)
rm(exFirma)

saveRDS(eSetMixFirma, "eSetMixFirma.RDS",compress = FALSE)

