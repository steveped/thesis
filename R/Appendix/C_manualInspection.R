library(BMEA)
library(limma)
library(plyr)
library(dplyr)
library(magrittr)
library(tibble)
library(reshape2)
library(ggplot2)
library(grid)
library(gtable)
library(snow)
library(readr)
library(xtable)
library(biomaRt)
library(gtools)
library(pheatmap)
library(RColorBrewer)

# load("phiMixture2016.RData")
load("phiMixture2018.RData")

setTabColNames <- function(x){
  x <- gsub("Slope", "$\\\\hat{\\\\beta_1}$", x)
  x <- gsub("^p$", "$p$", x)
  x <- gsub("^Z$", "$Z_j$", x)
  x <- gsub("FDR", "$FDR$", x)
  x <- gsub("phiLogFC", "$\\\\widehat{\\\\Delta \\\\log \\\\phi_j}$", x)
}

addCaption <- function(x){
  x[2] <- gsub("([^\\.]+\\.).+", "\\1", x[1])
  x
}

# The most highly ranked exons for each tissue are:
nTest <- 5
topExon <- phiSlopes %>% 
  left_join(refitEx) %>% 
  filter(ExonID %in% bestEx) %>% 
  left_join(phiZReduced) %>% 
  dplyr::select(ExonID, Tissue, 
                # phiLogFC = mean, 
                logFC,
                nProbes, Z, Slope, B, 
                p, adjP, FDR) %>% 
  # arrange(desc(abs(phiLogFC))) %>% 
  arrange(desc(abs(logFC))) %>%
  split(f = .$Tissue) %>% 
  lapply(slice, 1:nTest) %>% 
  bind_rows() %>% 
  extract2("ExonID")

cp <- paste("The five most highly ranked exons from each tissue based on the defined selection criteria.",
            "\n\tThe slope of the regression line across all mixture levels is given,",
            "along with the relevant $p$-value for $H_0: \\beta_1 =0$.",
            "\n\tHolm's method was used acros the complete set of slopes to adjust $p$-values,",
            "with asterisks indicating the significance of these values as per the standard conventions of R.",
            "\n\tAll exons were considered as having strongly confirmed non-zero slopes with $p_{adj} < 0.05$.")
cp %<>% addCaption()

phiSlopes %>% 
  left_join(refitEx) %>% 
  filter(ExonID %in% bestEx) %>% 
  left_join(phiZReduced) %>% 
  dplyr::select(ExonID, Tissue, 
                logFC,
                nProbes, Z, Slope, 
                p, adjP, FDR) %>% 
  mutate(adjP = p.adjust(p, "holm"),
         FDR = p.adjust(p, "fdr")) %>%
  arrange(logFC) %>%
  filter(ExonID %in% topExon) %>%
  split(f = .$Tissue) %>% 
  bind_rows() %>%
  mutate(` ` = if_else(adjP < 0.001, "***",
                       if_else(adjP < 0.01, "**",
                               if_else(adjP < 0.05, "*",
                                       if_else(adjP < 0.1, ".", ""))))) %>%
  dplyr::select(`Exon ID` = ExonID,
                Tissue, 
                # phiLogFC,
                logFC,
                Probes = nProbes,
                Z, 
                Slope, 
                # `SE(Slope)` = SE, 
                # p,
                `$p_{adj}$` = adjP,
                ` `) %>%
  xtable(digits = c(0, 0, 0, 2, 0, 1, 3, -2, 0),
         label = "tab:topExons",
         caption = cp) %>%
  print(include.rownames = FALSE,
        caption.placement = "top",
        table.placement = "p",
        size="small",
        sanitize.colnames.function = setTabColNames,
        add.to.row = list(pos = list(nTest), command = "\\midrule\n"),
        booktabs = TRUE)

topExonGn <- gsub("(ENSG[0-9]+)_[0-9]{3}", "\\1", topExon) %>% unique()
length(topExonGn)

refitEx %>% filter(EnsemblID %in% topExonGn)

############################
## The lower ranked exons ##
############################
lowExon <- phiSlopes %>% 
  left_join(refitEx) %>% 
  filter(ExonID %in% bestEx, 
         abs(B) < 8.6) %>% 
  left_join(phiZReduced) %>% 
  dplyr::select(ExonID, Tissue, 
                # phiLogFC = mean,
                logFC,
                nProbes, Z, B,
                Slope, p, adjP, FDR) %>% 
  # arrange(desc(abs(phiLogFC))) %>% 
  arrange(desc(logFC)) %>%
  split(f = .$Tissue) %>% 
  lapply(slice, 1:nTest) %>% 
  bind_rows() %>% 
  extract2("ExonID")

lowExonGn <- gsub("(ENSG[0-9]+)_[0-9]{3}", "\\1", lowExon) %>% unique() %>% setdiff(topExonGn)

phiSlopes %>% 
  left_join(refitEx) %>% 
  filter(ExonID %in% bestEx, 
         abs(B) < 8.6) %>% 
  left_join(phiZReduced) %>% 
  dplyr::select(ExonID, Tissue, 
                # phiLogFC = mean, 
                logFC,
                nProbes, Z, B,
                Slope, p, adjP, FDR) %>% 
  # arrange(desc(abs(phiLogFC))) %>% 
  arrange(desc(abs(logFC))) %>%
  mutate(` ` = if_else(adjP < 0.001, "***",
                       if_else(adjP < 0.01, "**",
                               if_else(adjP < 0.05, "*",
                                       if_else(adjP < 0.1, ".", ""))))) %>%
  dplyr::select(`Exon ID` = ExonID,
                Tissue, 
                # phiLogFC,
                logFC,
                Probes = nProbes,
                Slope, 
                p,
                FDR) %>%
  xtable(digits = c(0, 0, 0, 2, 0, 3, 3, 3),
         label = "tab:lowExons",
         caption = paste("The more lowly ranked exons from each tissue based on the defined selection criteria.",
                         "The slope of the regression line across all mixture levels is given,",
                         "along with the relevant $p$-value for $H_0: \\beta_1 =0$.",
                         "Raw $p$-values and FDR-adjusted $p$-values are shown.")) %>%
  print(include.rownames = FALSE,
        caption.placement = "top",
        table.placement = "p",
        size="small",
        sanitize.colnames.function = setTabColNames,
        booktabs = TRUE)


# Setup biomaRt to get gene names
mart <- useMart("ENSEMBL_MART_ENSEMBL", dataset = "hsapiens_gene_ensembl")
# Get the probe mappings to order the plots correctly.
mapFile <- file.path(getPath(ensCdf),  
                     paste0(getFullName(ensCdf), ",mapping.txt"))
mappings <- read_delim(mapFile, delim = "\t")
#Setup some more mixture colours
tisCols <- colorRampPalette(mixCols[c(2, 3, 1)])(7) %>% 
  set_names(levels(conditionsMixOnly))
# A hack for printing vectors
printVec <- function(vec){
  l <- length(vec)
  if (l<=1) return(vec)
  else return(
    paste(paste(vec[1:(l-1)], collapse = ", "),
          vec[l], sep = " and ")
  )
}
# Add asterisks based on confirmed (***), best (**) or failed criteria (*)
# addStars <- function(id, grp1 = confEx, grp2 = bestEx, grp3 = filter(phiZReduced, Z > 20)$ExonID){
#   id %<>% as.character()
#   if_else(id %in% grp1, paste0(id, "***"),
#           if_else(id %in% grp2, paste0(id, "**"), 
#                   if_else(id %in% grp3, paste0(id, "*"), id)))
#   
# }
addStars <- function(id, grp = bestEx){
  id %<>% as.character()
  i <- id %in% grp
  id[i] <- paste0(id[i], "*")
  id
}
addStats <- function(m){
  wmx <- colnames(m)[apply(m, 1, which.max)]
  wmn <- colnames(m)[apply(m, 1, which.min)]
  m <- as.data.frame(m)
  m$whichMax <- wmx
  m$whichMin <- wmn
  m
}

plotDir <- file.path("~", "thesis", "Images", "Appendix")

############################
## Step through each exon ##
############################
## The first 5 will be brain exons
celNames <- R.filesets::getNames(csNReduced)
i <- 8
tempID <- topExonGn[i]
grep(tempID, topExon, value = TRUE)
# grep(tempID, bestEx, value = TRUE)
tempUnit <- filter(refitEx, EnsemblID == tempID)$unit %>% unique()
(tempName <- getBM(attributes = "external_gene_name", 
                  filter = "ensembl_gene_id", 
                  values = tempID, 
                  mart = mart)$external_gene_name)
tempMappings <- mappings %>%
  filter(Unit == tempID) %>%
  mutate(cell = xy2indices(`Probe X`, `Probe Y`, nc = nbrOfColumns(ensCdf)))
tempUgc <- getUnitGroupCellMap(ensCdf, unit = tempUnit, retNames = TRUE) %>%
  left_join(dplyr::select(tempMappings, cell, Chr, bp = `Chr Start`), by = "cell") %>%
  as_data_frame()
tempInt <- getIntensities(csNReduced, indices = tempUgc$cell) %>%
  set_colnames(celNames) 

# The table of exons
phiSlopes %>% 
  filter(EnsemblID == tempID,
         ExonID %in% refitEx$ExonID) %>% 
  left_join(dplyr::select(refitEx, ExonID, logFC, LB)) %>% 
  left_join(phiZReduced) %>%
  mutate(` ` = if_else(FDR < 0.05, "\\checkmark", "\\ding{53}"),
         ExonID = addStars(ExonID),
         LB = sign(logFC)*LB) %>%
  dplyr::select(`Exon ID` = ExonID, phiLogFC = logFC, `CPI-LB` = LB, Z, Slope, p, FDR, ` `) %>% 
  xtable(digits = c(0, 0, 2, 3, 1, 3, -2, -2, 0),
         label = paste0("tab:", tempID),
         align ="ll|rrr|rrrl",
         caption = paste("All exon-level probesets from", tempID, paste0("(", tempName, ")"),
                         "considered as candidates for AS events based on the initial selection criteria in Section \\ref{subsec:selectCandidateAS}.\n",
                         "Initial point estimates of $\\Delta \\log \\phi_j$, the CPI-LB and exon-level $Z$-scores are shown,", 
                         "as these were utilised in selection of the high-confidence candidates.\n",
                         "High-confidence probesets are indicated with an asterisk.\n",
                         "Exons with a confirmed non-zero slope based on $FDR$-adjusted $p$-values $<0.05$ are indicated with a tick.\n",
                         "A negative slope indicates a putative brain-specific exon,",
                         "whilst a positive slope indicates a putative heart-specific exon.\n",
                         if_else(sum(grepl(tempID, topExon)) == 1,
                                 paste0("The specific exon belonging to the ", nTest, " most highly ranked exons for each tissue is ",
                                 printVec(grep(tempID, topExon, value=TRUE)),
                                 "."),
                                 paste0("The exons belonging to the ", nTest, " most highly ranked exons for each tissue are ",
                                 printVec(grep(tempID, topExon, value=TRUE)),
                                 "."))
                         # "whilst a positive slope indicates a putative heart-specific exon."
                         ))%>%
  print(include.rownames = FALSE,
        caption.placement = "top",
        comment = FALSE,
        table.placement = "ht",
        size = "small",
        sanitize.text.function = c,
        sanitize.colnames.function = setTabColNames)

tempLambda <- getIntensities(bgCsReduced$lambda, indices = tempUgc$cell) %>%
  log2() %>%
  rowMeans() %>%
  cbind(tempUgc) %>%
  set_names(c("lambda", names(.)[-1])) %>%
  mutate(Probe = seq(nrow(.))) %>%
  filter(group %in% refitEx$ExonID) %>%
  mutate(group = addStars(group),
         Exon = gsub(".+_(.+)", "\\1", group),
         Exon = factor(Exon, levels=  unique(Exon))) %>%
  tbl_df()
tempDelta <- getIntensities(bgCsReduced$delta, indices = tempUgc$cell) %>%
  log2() %>%
  raise_to_power(2) %>%
  rowMeans() %>%
  sqrt() %>%
  cbind(tempUgc) %>%
  set_names(c("delta", names(.)[-1])) %>%
  mutate(Probe = seq(nrow(.))) %>%
  filter(group %in% refitEx$ExonID) %>%
  mutate(group = addStars(group),
         Exon = gsub(".+_(.+)", "\\1", group),
         Exon = factor(Exon, levels=  unique(Exon))) %>%
  tbl_df()
cv <- 2
fs <- 11
# Plot the candidate exons by probe
colnames(tempInt) %>%
  lapply(function(x){
    tempUgc %>% mutate(Sample = x,
                       Intensities = log2(tempInt[,x]),
                       Probe = seq(nrow(.))) %>%
      arrange(bp)
  }) %>%
  bind_rows() %>%
  as_data_frame() %>%
  filter(group %in% refitEx$ExonID) %>%
  filter(group == "ENSG00000149294_028") %>%
  left_join(phiZReduced, by = c("group" = "ExonID")) %>%
  mutate(mixLevel = if_else(grepl("mix1", Sample), "Heart",
                            if_else(grepl("mix5", Sample), "Equal",
                                    "Brain")),
         group = addStars(group),
         Exon = gsub(".+_(.+)", "\\1", group)) %>%
  mutate(Exon = factor(Exon, levels = unique(Exon))) %>%
  # mutate(grid = if_else(Exon %in% levels(Exon)[1:6], 1,
  #                if_else(Exon %in% levels(Exon)[7:12], 2, 
  #                        if_else(Exon %in% levels(Exon)[13:21], 3, 4)))) %>%
  # filter(grid == 4) %>%
  left_join(tempLambda) %>%
  left_join(tempDelta) %>%
  ggplot(aes(x = Probe, y = Intensities)) +
  geom_point(aes(colour = mixLevel), shape = 21) +
  geom_line(aes(group = Sample, colour = mixLevel)) +
  geom_rect(aes(xmin = Probe - 0.45,
                xmax = Probe + 0.45,
                ymin = lambda - cv*delta,
                ymax = lambda + cv*delta),
            fill = "grey",
            alpha = 0.04) +
  scale_colour_manual(values = mixCols) +
  theme_bw() +
  theme(legend.position = "top") + 
  guides(colour = FALSE) +
  facet_grid(~Exon, scales = "free_x", space = "free", switch = "x") +
  # scale_x_continuous(expand = c(0.01, 0)) +
  scale_y_continuous(expand = c(0.03, 0)) +
  labs(x = "Exon Probeset",
       y = expression(paste(log[2], PM[hijk]))) + 
  theme(axis.text.x = element_blank(), 
        axis.text.y = element_text(size = fs),
        axis.title = element_text(size = fs + 1),
        axis.ticks.x = element_blank(),
        strip.text = element_text(size = fs - 1),
        panel.spacing = unit(0.005, "npc"),
        panel.grid = element_blank())
ggsave(paste0(tempID, "byProbe.pdf"),
       width = 11, height = 4.5, units = "in", path = plotDir)
# pdf(file.path(plotDir, paste0(tempID, "byProbe.pdf")),
#     width = 7, height = 7)
# grid.newpage()
# vp1 <- viewport(1, 0.76, .955, 0.24, just = c(1, 0))
# vp2 <- viewport(1, 0.52, 1, 0.24, just = c(1, 0))
# vp3 <- viewport(1, 0.28, .955, 0.24, just = c(1, 0))
# vp4 <- viewport(1, 0, 0.955, 0.28, just = c(1, 0))
# print(p1 + theme(axis.text.x = element_blank(),
#                  axis.text.y = element_text(size = fs),
#                  axis.title = element_text(size = fs + 1),
#                  axis.ticks.x = element_blank(),
#                  strip.text = element_text(size = fs),
#                  panel.margin = unit(0.005, "npc"),
#                  panel.grid = element_blank()), vp = vp1)
# print(p2 + theme(axis.text.x = element_blank(),
#                  axis.text.y = element_text(size = fs),
#                  axis.title = element_text(size = fs + 1),
#                  axis.ticks.x = element_blank(),
#                  strip.text = element_text(size = fs),
#                  panel.margin = unit(0.005, "npc"),
#                  panel.grid = element_blank()), vp = vp2)
# print(p3 +theme(axis.text.x = element_blank(),
#                 axis.text.y = element_text(size = fs),
#                 axis.title = element_text(size = fs + 1),
#                 axis.ticks.x = element_blank(),
#                 strip.text = element_text(size = fs),
#                 panel.margin = unit(0.005, "npc"),
#                 panel.grid = element_blank()), vp = vp3)
# print(p4 +theme(axis.text.x = element_blank(),
#                 axis.text.y = element_text(size = fs),
#                 axis.title = element_text(size = fs + 1),
#                 axis.ticks.x = element_blank(),
#                 strip.text = element_text(size = fs),
#                 panel.margin = unit(0.005, "npc"),
#                 panel.grid = element_blank()), vp = vp4)
# dev.off()
# Save the plot & run pdfcrop
system2("pdfcrop", 
        paste(file.path(plotDir, paste0(tempID, "byProbe.pdf"))[c(1, 1)], collapse = " "))
# Print the caption
prbCap <- c("\\begin{figure}[ht]",
             "\\centering",
             paste0("\\includegraphics[width=0.95\\linewidth]{Appendix/", tempID, "byProbe}"),
             paste0("\\caption{", tempName, " raw probe intensities for exons in the initial list of candidates for fitting across all arrays."),
             "Only the 100\\% Heart (red), 50:50 (grey) and 100\\% Brain (blue) samples are shown.",
             "Asterisks next to the probeset number indicate a high-confidence probeset.",
            "The values for $\\hat{\\lambda}_{..jk} \\pm 2\\hat{\\delta}_{..jk}$ as averaged across all arrays are overlaid as grey rectangles.}",
             paste0("\\label{fig:", tempID, "byProbe}"),
             "\\end{figure}")
cat(prbCap, sep= "\n\t")

# Show the posterior medians for each exon
parSub <- parFitPhi %>%
  vapply(function(x){tempID %in% x$units$unitName}, logical(1)) %>%
  which
exRows <- addStars(levels(tempUgc$group))
# Have a look first
parFitPhi[[parSub]]$summaries %>% 
  extract2(tempID) %>% 
  extract(35:nrow(.),"50%") %>% 
  matrix(ncol = 7) %>% 
  set_rownames(exRows) %>% 
  set_colnames(names(mixLevels)) %>%
  extract(rownames(.) %in% addStars(refitEx$ExonID),) %>%
  addStats
# Plot the posterior medians for each exon
trans <- FALSE
plotName <- file.path(plotDir, paste0(tempID, "byExon.pdf"))
pdf(plotName, height = 11, width =6, onefile = FALSE)
parFitPhi[[parSub]]$summaries %>% 
  extract2(tempID) %>% 
  extract(35:nrow(.),"50%") %>% 
  matrix(ncol = 7) %>% 
  set_rownames(exRows) %>% 
  set_colnames(names(mixLevels)) %>%
  extract(rowSums(.) > 0,) %>%
  pheatmap(
    color = colorRampPalette(rev(brewer.pal(n=7, name = "Greys")))(50),
    cluster_rows = FALSE,
    cluster_cols = FALSE,
    cellwidth = 20,
    cellheight = 14,
    fontsize = 11,
    # fontsize_row = 7.5,
    # fontsize_col = 8,
    # annotation_col = data.frame(Mixture = mixLevels),
    annotation_col = {if (trans) NA
      else data.frame(Mixture = mixLevels) },
    annotation_row = {if(trans) data.frame(Mixture = mixLevels)
      else NA},
    annotation_colors = list(Mixture = tisCols),
    annotation_legend = FALSE
  )
dev.off()
system2("pdfcrop", rep(plotName, 2))
# Print the caption
exCap <- c("\\begin{figure}[ht]",
            "\\centering",
            paste0("\\includegraphics[width=0.6\\linewidth]{Appendix/", tempID, "byExon}"),
           paste0("\\caption{Posterior medians as point estimates for $\\phi_{hj}$ across all exon-level probesets and tissue mixtures for ",
                 tempName, "."),
           "Asterisks next to the probeset number indicate a high-confidence probeset.}",
           paste0("\\label{fig:", tempID, "byExon}"),
            "\\end{figure}")
cat(exCap, sep= "\n\t")

# The UCSC figure
ucscCap <- c("\\begin{landscape}",
             "\\begin{figure}[ht]",
           "\\centering",
           paste0("\\includegraphics[width=0.9\\linewidth]{Appendix/", tempID, "UCSC}"),
           paste0("\\caption{UCSC Genome Browser plot for ", tempName, " along with exon-level probesets as defined on the custom CDF, as well as the original Affymetrix probesets."),
           "Putative heart-specific exons are shown in red on the custom CDF track, whilst putative brain-specific exons are shown in blue.",
           "All exons with an initial $B$-statistic $>8$ are shown in colour, whilst any undetectable exons are shown in grey.}",
           paste0("\\label{fig:", tempID, "UCSC}"),
           "\\end{figure}",
           "\\end{landscape}")
cat(ucscCap, sep= "\n\t")

# The results template table
resCap <- paste0("Results from inspection of probes, probesets and possible transcripts for ",
                 tempID, ".\n", 
                 "A dash (-) in the Probes or Probeset columns indicates the expected pattern is unclear, but not violated.\n",
                 "The abbreviation ``Inc'' in the Status column indicates the results are inconclusive.")
# High confidence exons
refitEx %>%
  filter(EnsemblID == tempID,
         ExonID %in% bestEx) %>%
  dplyr::select(EnsemblID, ID = ExonID) %>%
  mutate(EnsemblID = rep(c(tempID, ""), times = c(1, nrow(.) -1)),
         ID = gsub(paste0(tempID, "_"), "", ID),
         Probe = "\\checkmark", 
         Probeset = "\\checkmark",
         Transcript = "",
         Status = "\\checkmark") %>%
  xtable(align = "lllllll") %>%
  print(include.rownames = FALSE,
        size = "small",
        booktabs = TRUE,
        sanitize.text.function = c)

# The low confidence ones
refitEx %>%
  filter(EnsemblID == tempID,
         !ExonID %in% bestEx) %>%
  dplyr::select(EnsemblID, ID = ExonID) %>%
  mutate(EnsemblID = tempID,
         ID = gsub(paste0(tempID, "_"), "", ID),
         Probe = "\\ding{53}", 
         Probeset = "\\ding{53}",
         Transcript = "",
         Status = "\\ding{53}") %>%
  xtable(align = "lllllll") %>%
  print(include.rownames = FALSE,
        size = "small",
        booktabs = TRUE,
        sanitize.text.function = c)

save.image("phiMixture2018.RData")
