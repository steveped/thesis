## The contains the functions for running the MCMC process in R
## for the BMEA model with a mixture prior.

# The functions contained in this file are:
# 1 - initialiseValues
# 2 - updateValues
# 3 - sigma.S.update
# 4 - probe.update (p.k & sigma.p)
# 5 - exp.update (The expression level terms c, mu & sigma.mu)
# 6 - f1 (Update the first component of the mixture)
# 7 - f2 (The 2nd component)
# 8 - f3 (The 3rd component)
# 9 - multi.samp (sample from a Dirichlet)
# 10 - dir.samp (sample from a multinomial)
# 11/12 - g2.single/g2.vec
# 13/14 - g3.single/g3.vec

# In all functions:
# PM = probe data on the linear scale
# old = a list of parameters for iteration t-1 with components 
#       $S, $c.hi, $mu.h, $sigma.mu, $p.k, $sigma.p, $phi.hj & $sigma.S
# indices = the list of the structure of the probes, chips, exons etc
#           Has components $H, $I, $I.h, $J, $K & $K.j
# conditions = the conditions as a vector of factors
# model.matrices = a list with slots $exons & $cond
# B.prior = a list with vectors $mean & $sd as components

initialiseValues <- function(PM, indices, exons, conditions, model.matrices, B.prior){
  
  # Generate initial values for each parameter.
  # The order is important as some parameters are used for generation of subsequent parameters
  
  # Choose random values from the BG prior & subtract these from PM
  S <- PM - matrix(exp(rtnorm(indices$I*indices$K, B.prior$mean, B.prior$sd, upper=as.vector(log(PM)))),nrow=indices$K,ncol=indices$I,byrow=FALSE)
  
  # Chooses a random sample from the non-zero signal estimates on each array
  c.hi <- log(S[sample(1:indices$K, indices$I, replace=TRUE) + ((1:indices$I)-1)*indices$K])
  # Take the average of the c.hi values for each condition
  mu.h <- as.vector(t(model.matrices$cond)%*%c.hi/indices$I.h)
  # Take the sd of the c.hi values, with a max value of 5
  sigma.mu <- min(sd(c.hi-mu.h[as.numeric(conditions)]),5)
  
  # Choose random values from a U(0,1) distribution
  phi.hj <- matrix(runif(indices$H*indices$J,0,1),nrow=indices$J,ncol=indices$H)
  # phi.hj <- matrix(1,nrow=indices$J,ncol=indices$H)
  # Choose random values from a Dir(1,1,1)
  q.hj <- array(rdirichlet(indices$J*indices$H,c(1,1,1)),dim=c(indices$J,indices$H,3))
  
  # Calculate p.k using the randomly calculated values for S, c.hi & phi.hj
  p.k <- rowMeans(log(S)-matrix(c.hi,nrow=indices$K,ncol=indices$I,byrow=TRUE)-matrix(log(phi.hj[as.numeric(exons),as.numeric(conditions)]),nrow=indices$K,ncol=indices$I))
  # The estimate of sd from the actual numbers with max=10
  sigma.p <- min(sd(p.k),10)
  
  # The sd of the combined estimates from above, with max=10
  sigma.S <- min(sd(as.vector(log(S) - matrix(c.hi,nrow=indices$K,ncol=indices$I,byrow=TRUE) - matrix(p.k,nrow=indices$K,ncol=indices$I,byrow=FALSE) - matrix(log(phi.hj[as.numeric(exons),as.numeric(conditions)]),nrow=indices$K,ncol=indices$I))),10)
  
  # Output a list of the values
  list(S=S, c.hi=c.hi, mu.h=mu.h, sigma.mu=sigma.mu, p.k=p.k, sigma.p=sigma.p, phi.hj=phi.hj, q.hj=q.hj, sigma.S=sigma.S)
  
}

updateValues <- function(PM, old, indices, exons, conditions, model.matrices, B.prior, 
                         f3.hj, sigma.jump.p=1, sigma.jump.mu=1) {

  # f3.hj: A vector with the f3 values for each given probe
  # sigma.jump.p: The variance for the proposal generating distribution for sigma.p. Adapted in the early stages of the higher level MCMC process
  # sigma.jump.mu: The variance for the proposal generating distribution for sigma.mu. Adapted in the early stages of the higher level MCMC process
  
  new <- mixture.update(PM=PM, old=old, indices=indices, exons=exons, conditions=conditions, model.matrices=model.matrices, B.prior=B.prior, f3.hj=f3.hj)
  new <- sigma.S.update(PM=PM, old=new, indices=indices, exons=exons, conditions=conditions)
  new <- probe.update(PM=PM, old=new, indices=indices, exons=exons, conditions=conditions, sigma.jump=sigma.jump.p)
  new <- exp.update(PM=PM, old=new, indices=indices, exons=exons, conditions=conditions, model.matrices=model.matrices, sigma.jump=sigma.jump.mu)
  
  return(new) #
  
}

sigma.S.update <- function(PM, old, indices, exons, conditions){
  
  x.hijk <- log(old$S) - matrix(old$c.hi, nrow=indices$K, ncol=indices$I, byrow=TRUE) - matrix(old$p.k, ncol=indices$I, nrow=indices$K, byrow=FALSE) - log(old$phi.hj[as.numeric(exons), as.numeric(conditions)]) # Again with the K*I matrices. This is effectively y.hijk-mu.hijk from the derivation
  x.hijk[which(old$phi.hj[as.numeric(exons), as.numeric(conditions)]==0)] <- NaN # Any values for phi.hj==0 must be set to NaN
  
  K.hi <- indices$K - colCounts(is.na(x.hijk)) # Gets the number of probes to be assessed for each chip
  nu <- sum(K.hi) # The degrees of freedom
  s.sq <- sum(x.hijk^2,na.rm=TRUE)/nu # The scaling factor
  
  old$sigma.S <- sqrt(nu*s.sq / rchisq(n=1, df=nu)) # This is a sample from a scaled inverse-chi-squared distribution
  
  return(old) # Output the original object, but with the updated value for sigma.S
}

probe.update <- function(PM, old, indices, exons, conditions, sigma.jump) {

  # sigma.jump: The variance for the proposal generating distribution for sigma.p. Adapted in the early stages of the higher level MCMC process
  
  x.hijk <- log(old$S) - matrix(old$c.hi, nrow=indices$K, ncol=indices$I, byrow=TRUE) - log(old$phi.hj[as.numeric(exons), as.numeric(conditions)])
  x.hijk[which(old$phi.hj[as.numeric(exons), as.numeric(conditions)]==0)] <- NaN # Any values for phi.hj==0 must be set to NaN
  xbar.k <- rowMeans(x.hijk,na.rm=TRUE) # Find the row means, but ignore any that are NA (i.e. S.hat = 0)
  non.zero <- which(!is.na(xbar.k)) # Index the rows for which both values of phi.hj are non.zero
  
  # Update the p.k values
  I.k <- indices$I - rowCounts(is.na(x.hijk)) # Gets the number of chips to be assessed for each probe
  sigma.k <- old$sigma.S / sqrt(I.k)
  V.k <- (old$sigma.p^2)*(sigma.k^2) / (old$sigma.p^2 + sigma.k^2) # The variances
  theta.k <- V.k*xbar.k / (sigma.k^2) # The vector of posterior means
  old$p.k[non.zero] <- rnorm(n=length(non.zero), mean=theta.k[non.zero], sd=sqrt(V.k[non.zero])) # Update the p.k values for which there is information
  
  # If there are any exons with only one probe, these values of p.k must be set to zero
  if (min(indices$K.j)==1) {
    single.probes <- which(indices$K.j==1) # Find the exons with only one probe
    for (j in single.probes){
      old$p.k[which(exons==levels(exons)[j])] <- 0 # Set the corresponding p.k value to 0
    }
  }
  
  # Update sigma.p using the Metropolis algorithm, but only using the probes for which phi.hj!=0 for all h
  sigma.k <- sigma.k[non.zero]
  xbar.k <- xbar.k[non.zero]
  x0 <- old$sigma.p # The old value
  x1 <- rnorm(n=1, mean=x0, sd=sigma.jump)  # The proposed new value, centred at the old value
  
  log.p <- sum(-log(sigma.k^2 + x0^2)/2 - xbar.k^2 / (2*(sigma.k^2 + x0^2))) # The log p-value for the old value
  log.p.star <- sum(-log(sigma.k^2 + x1^2)/2 - xbar.k^2 / (2*(sigma.k^2 + x1^2))) # The log p-value for the new value
  
  # Calculate the sampling probability back on the linear scale, ensuring the proposed value cannot be outside the bounds of the prior
  r <- ifelse(x1<=0 || x1>=10, 0, min(exp(log.p.star - log.p ), 1))
  old$sigma.p <- sample(x=c(x0,x1), size=1, prob=c(1-r, r)) # Use the sampling probability to accept the new value, or keep the old
  
  return(old) # Output the original object, but with the updated values for p.k & sigma.p
  
}

exp.update <- function(PM, old, indices, exons, conditions, model.matrices, sigma.jump) {

  # sigma.jump: The variance for the proposal generating distribution for sigma.mu. Adapted in the early stages of the higher level MCMC process
  
  x.hijk <- log(old$S) - matrix(old$p.k, ncol=indices$I, nrow=indices$K, byrow=FALSE) - log(old$phi.hj[as.numeric(exons), as.numeric(conditions)])
  x.hijk[which(old$phi.hj[as.numeric(exons), as.numeric(conditions)]==0)] <- NaN # Any values for phi.hj==0 must be set to NaN
  x.hijk[which(x.hijk==-Inf)] <- NaN # Also set any which have no apparent signal as NaN. This should only happen in the early iterations
  xbar.hi <- colMeans(x.hijk, na.rm=TRUE) # The chip means for the defined value 'x'
  
  K.hi <- indices$K - colCounts(is.na(x.hijk)) # The number of probes to be included for each chip
  K.h <- as.vector(t(model.matrices$cond)%*%K.hi/indices$I.h)
  sigma.hi <- old$sigma.S/sqrt(K.hi) # The variance for each chip
  sigma.h <- old$sigma.S/sqrt(K.h)
  
  # Sigma.mu
  x0 <- old$sigma.mu # The current value
  x1 <- rnorm(n=1, mean=x0, sd=sigma.jump) # Generate a proposed new value centred at the old value
  s.sq <- as.vector(t(model.matrices$cond)%*%(xbar.hi-old$mu.h[as.numeric(conditions)])^2)
  log.p <- sum(-(indices$I.h-1)*log(sigma.h^2+x0^2)/2 - s.sq/(2*(sigma.h^2+x0^2))) # proportional to log.p for the current value
  log.p.star <- sum(-(indices$I.h-1)*log(sigma.h^2+x1^2)/2 - s.sq/(2*(sigma.h^2+x1^2))) # proportional to log.p for the proposed value
  r <- ifelse(x1<=0 || x1>=5, 0, min(exp(log.p.star - log.p), 1)) # Make sure that rejection occurs if the proposed value is outside the bounds of the prior
  old$sigma.mu <- sample(x=c(x0,x1), size=1, prob=c(1-r,r)) # Sample either the old or new value using 'r' as the acceptance probability
  
  # The expression levels (mu.h)
  mu.hat.h <- as.vector(t(model.matrices$cond)%*%xbar.hi/indices$I.h)
  V.h <- (sigma.h^2+old$sigma.mu^2)/indices$I.h
  # Sample from the truncated normal distribution & set the object value as the sampled one
  old$mu.h <- rtnorm(n=indices$H, mean=mu.hat.h, sd=sqrt(V.h), lower=0, upper=log(2^16)) # Returns a vector of length H
  
  # The chip effects
  V.hi <- 1 / (1/sigma.hi^2 + 1/old$sigma.mu^2) # The posterior variance
  theta.hat.hi <- V.hi*( xbar.hi/sigma.hi^2 + old$mu.h[as.numeric(conditions)]/old$sigma.mu^2 )
  # Sample from a truncated normal distribuution
  old$c.hi <- rtnorm(n=indices$I, mean=theta.hat.hi, sd=sqrt(V.hi), lower=0, upper=log(2^16)) # Outputs a vector of length I
  
  return(old) # Output the signal estimates, with the updated values for c.hi, mu.h & sigma.mu
  
}

f1 <- function(PM, old, indices, B.prior){
  # Calculates the integral for f1 using Simpson's rule. 
  
  PM <- as.vector(PM) # Vectorise the observed values
  S.mean <- rep(old$c.hi, each=indices$K)+ rep(old$p.k, times=indices$I) # Vectorise the chip + probe effects
  sigma.S <- old$sigma.S # Leave as a scalar
  
  f1.single <- function(PM, B.mean, B.sd, S.mean, sigma.S,n=256){
    
    # Integrate using Simpson's rule as the adaptive quadrature crashes...
    h <- PM/n
    x <- seq(0,PM,length.out=n)
    x.evens <- x[which((1:(n-1))/2==as.integer((1:(n-1))/2))] # The even numbered values on 1:n
    x.odds <- x[which((1:(n-1)/2)!=as.integer((1:(n-1))/2))] # The odd numbered values on 1:n
    # And the output is (noting that f(0)==f(PM)==0:
    out <- (h/3)*(4*sum(dlnorm(PM-x.odds, B.mean, B.sd)*dlnorm(x.odds, S.mean, sigma.S)) + 2*sum(dlnorm(PM-x.evens, B.mean, B.sd)*dlnorm(x.evens, S.mean, sigma.S)))
    return(out)
      
  }
  f1.vec <- Vectorize(f1.single) # Redefine the above to handle vector inputs
  
  f1 <- f1.vec(PM=PM, B.mean=B.prior$mean, B.sd=B.prior$sd, S.mean=S.mean, sigma.S=sigma.S) # Output the results as a vector
  matrix(f1, nrow=indices$K, ncol=indices$I, byrow=FALSE) # And set as a matrix
}

f2 <- function(PM, old, indices, B.prior){
  # Calculates the integral for f2 using the Simpson's Rule
  
  PM <- as.vector(PM) # Vectorise the observed values
  c.hi <- rep(old$c.hi, each=indices$K)
  p.k <- rep(old$p.k, times=indices$I) # Vectorise the chip + probe effects
  sigma.S <- old$sigma.S # Leave as a scalar
  
  f2.single <- function(PM, c.hi, p.k, sigma.S, B.mean, B.sd, n=256){
    # Nest the integrand function within the function
    f2.integrand <- function(S, PM, c.hi, p.k, sigma.S, B.mean, B.sd) {
      pnorm(-(log(S)-c.hi-p.k)/sigma.S-sigma.S)*exp(0.5*sigma.S^2-c.hi-p.k-0.5*((log(PM-S)-B.mean)/B.sd)^2) / ((PM-S)*B.sd*sqrt(2*pi))
    }
    
    # Now integrate over the range 0 to PM for a single value using Simpson's rule
    h <- PM/n
    x <- seq(0,PM,length.out=n)
    x.evens <- x[which((1:(n-1))/2==as.integer((1:(n-1))/2))] # The even numbered values on 1:n
    x.odds <- x[which((1:(n-1)/2)!=as.integer((1:(n-1))/2))] # The odd numbered values on 1:n
    # And the output is (noting that f(0)==f(PM)==0:
    out <- (h/3)*(4*sum(f2.integrand(S=x.odds,PM=PM, c.hi=c.hi, p.k=p.k, sigma.S=sigma.S, B.mean=B.mean, B.sd=B.sd)) + 2*sum(f2.integrand(S=x.evens,PM=PM, c.hi=c.hi, p.k=p.k, sigma.S=sigma.S, B.mean=B.mean, B.sd=B.sd)))
    return(out)
    
  }
  f2.vec <- Vectorize(f2.single) # Redefine the above to handle vector inputs
  
  f2 <- f2.vec(PM=PM, c.hi=c.hi, p.k=p.k, sigma.S=sigma.S, B.mean=B.prior$mean, B.sd=B.prior$sd) # Output the results as a vector
  matrix(f2, nrow=indices$K, ncol=indices$I, byrow=FALSE) # And return a matrix
}

f3 <- function(PM, indices, B.prior) {
  # This is the function for processing a matrix of values for PM for zeta3
  
  PM <- as.vector(PM) # Convert all the observed data to a vector
  lambda.k <- as.vector(matrix(B.prior$mean, nrow=indices$K, ncol=indices$I, byrow=FALSE))
  sigma.k <- as.vector(matrix(B.prior$sd, nrow=indices$K, ncol=indices$I, byrow=FALSE))
  f3 <- matrix(dlnorm(PM,lambda.k,sigma.k),nrow=indices$K, ncol=indices$I,byrow=FALSE)
  return(f3)
}

mixture.update <- function(PM, old, indices, exons, conditions, model.matrices, B.prior, f3.hj) {
  # Updates the terms involved with the mixture model, i.e. S, q.hj & phi.hj
  
  # f3.hj: A vector with the f3 values for each given probe
  
  # The functions f1 & f2 must be in the workspace
  
  # This first step is to find gamma.hj= E(zeta.hj). Use the functions f1 & f2 to find these values for each probe
  # then use the model matrices to estimate for each combination of hj
  f1.hj <- exp(t(t(model.matrices$cond) %*%t(t(model.matrices$exons) %*%log(f1(PM=PM, old=old, indices=indices, B.prior=B.prior)))))
  f2.hj <- exp(t(t(model.matrices$cond) %*%t(t(model.matrices$exons) %*%log(f2(PM=PM, old=old, indices=indices, B.prior=B.prior)))))
  numer <- old$q.hj*abind(f1.hj, f2.hj, f3.hj, along=3) # Form an array with all the values as the numerators
  denom <- matrix(rowSums(matrix(numer,ncol=3)), ncol=indices$H, byrow=FALSE) # The denominators
  gamma.hat <- numer / array(denom, dim=c(indices$J,indices$H,3)) # This is gamma hat as a J*H*3 array
  
  # Do not permit all exons to be absent
  old.cls <- matrix(2, ncol=indices$H, nrow=indices$J)
  old.cls[which(old$phi.hj==1)] <- 1
  old.cls[which(old$phi.hj==0)] <- 3
  cls.hj <- matrix(apply(X=cbind(as.vector(gamma.hat[,,1]),as.vector(gamma.hat[,,2]),as.vector(gamma.hat[,,3])),MARGIN=1,FUN=multi.samp),nrow=indices$J,byrow=FALSE)
  r <- 1 # The acceptance probability
  for (h in 1:indices$H) {
    if (length(which(cls.hj[,h]==3))==indices$J) r <- 0
  }
  if (r==0) cls.hj <- old.cls # Use the old clasifications if rejecting
  
  # Define class variables denoting which probe is in which class.
  c1 <- which(cls.hj[as.numeric(exons),as.numeric(conditions)]==1) # phi.hj=1
  c2 <- which(cls.hj[as.numeric(exons),as.numeric(conditions)]==2) # phi.hj~U(0,1)
  c3 <- which(cls.hj[as.numeric(exons),as.numeric(conditions)]==3) # phi.hj=0
  
  # Update q.hj now we have zeta.hj
  zeta.hj <- array(0,dim=c(indices$J,indices$H,3))
  zeta.hj[,,1][which(cls.hj==1)] <- 1
  zeta.hj[,,2][which(cls.hj==2)] <- 1
  zeta.hj[,,3][which(cls.hj==3)] <- 1
  zeta.hj <- cbind(as.vector(zeta.hj[,,1]),as.vector(zeta.hj[,,2]),as.vector(zeta.hj[,,3]))
  q.hj <- t(apply(X=zeta.hj+1,MARGIN=1,FUN=dir.samp))
  for (n in 1:3){
    old$q.hj[,,n] <- matrix(q.hj[,n],nrow=indices$J,byrow=FALSE)
  }
  
  # Generate the proposal values for S.star by sampling from PM-B.star, where B.star ~ Trunc-LogN(.); B.star<PM
  S.star <- PM - matrix(exp(rtnorm(indices$I*indices$K, B.prior$mean, B.prior$sd, upper=as.vector(log(PM)))),nrow=indices$K, ncol=indices$I, byrow=FALSE)
  S.star[c3] <- 0 # If a probe is in group 3, S must be zero
  p.S.t1 <- p.S.star <- matrix(0,nrow=indices$K,ncol=indices$I) # Generate a blank matrix for these
  
  # Calculate the pdf values for c1
  if (length(c1)!=0) {
    p.S.t1[c1] <- dlnorm(PM[c1]-old$S[c1], rep(B.prior$mean,times=indices$I)[c1], rep(B.prior$sd,times=indices$I)[c1])*dlnorm(old$S[c1], (rep(old$c.hi,each=indices$K)-rep(old$p.k,times=indices$I))[c1], old$sigma.S)
    p.S.star[c1] <- dlnorm(PM[c1]-S.star[c1], rep(B.prior$mean,times=indices$I)[c1], rep(B.prior$sd,times=indices$I)[c1])*dlnorm(S.star[c1], (rep(old$c.hi,each=indices$K)-rep(old$p.k,times=indices$I))[c1], old$sigma.S)
  }
  
  # The pdf values for c2
  if(length(c2)!=0) {
    p.S.t1[c2] <- g2.vec(PM=PM[c2], S=old$S[c2], c.hi=rep(old$c.hi,each=indices$K)[c2], p.k=rep(old$p.k,times=indices$I)[c2], sigma.S=old$sigma.S, B.mean=rep(B.prior$mean,times=indices$I)[c2],B.sd=rep(B.prior$sd,times=indices$I)[c2])
    p.S.star[c2] <- g2.vec(PM=PM[c2], S=S.star[c2], c.hi=rep(old$c.hi,each=indices$K)[c2], p.k=rep(old$p.k,times=indices$I)[c2], sigma.S=old$sigma.S, B.mean=rep(B.prior$mean,times=indices$I)[c2],B.sd=rep(B.prior$sd,times=indices$I)[c2])
  }
  
  # c3 needs to just set p.values to zero or 1
  if (length(c3)!=0) {
    p.S.t1[c3] <- g3.vec(old$S[c3])
    p.S.star[c3] <- g3.vec(S.star[c3])
  }
  
  # Calculate the acceptance probabilities for S.star
  r.hijk <- p.S.star/p.S.t1
  r.hijk[which(r.hijk>1)] <- 1
  acc <- which(rbinom(indices$I*indices$K,1,r.hijk)==1) # Now use the sampling probabilites to generate acceptances
  old$S[acc] <- S.star[acc] # Change the accepted values to the proposal ones
  
  # Update phi for c2
  if (length(c2)!=0) {
    x.hijk <- matrix(0, nrow=indices$K, ncol=indices$I)
    x.hijk[c2] <- log(old$S[c2])- matrix(old$c.hi,nrow=indices$K,ncol=indices$I,byrow=TRUE)[c2] - matrix(old$p.k, nrow=indices$K, ncol=indices$I, byrow=FALSE)[c2]
    xbar.hj <- (t(t(x.hijk) %*% model.matrices$exons) %*% model.matrices$cond) / indices$K.j / indices$I.h # The means for each hj
    sigma.hj <- old$sigma.S/sqrt(t(t(indices$K.j))%*%indices$I.h)
    mu.hj <- xbar.hj + sigma.hj^2
    c2.hj <- which(cls.hj==2)
    old$phi.hj[c2.hj] <- exp(rtnorm(length(c2.hj),mu.hj[c2.hj],sigma.hj[c2.hj],upper=0)) # phi.hj~U(0,1)
  }
  
  # Update phi.hj for groups 1 & 3
  old$phi.hj[which(cls.hj==1)] <- 1 # phi.hj=1
  old$phi.hj[which(cls.hj==3)] <- 0 # phi.hj=0
  
  # Return the updated 'old' object
  return(old)
  
}

multi.samp <- function(gamma.hat){ # gives the class for each hj
  which(rmultinom(1,1,gamma.hat)==1)
}

dir.samp <- function(zeta) {
  rdirichlet(1,zeta)
}

g2.single <- function(PM, S, c.hi, p.k, sigma.S, B.mean, B.sd) {
  ifelse(S==0, 0, 
         pnorm(-(log(S)-c.hi-p.k)/sigma.S-sigma.S)*exp(0.5*sigma.S^2-c.hi-p.k-0.5*((log(PM-S)-B.mean)/B.sd)^2) / ((PM-S)*B.sd*sqrt(2*pi)))
}
g2.vec <- Vectorize(g2.single)

g3.single <- function(S) {
  ifelse(S==0 ,1, 0)
}
g3.vec <- Vectorize(g3.single)

BMEA.MCMC.Dirichlet <- function(PM, inits=NULL, conditions, exons, B.prior, n.iter, n.burnin=NULL, n.chains, n.keep=1000, adapt=TRUE, sigma.jump.mu=1, sigma.jump.p=1, contr.matrix=NULL, parameters.to.save=c("c.hi","mu.h","phi.hj")){
  
  # n.iter: The required number of iterations
  # n.burnin: The number of burnin iterations. Defaults to the first 50%
  # n.chains: The number of parallel chains to run
  # n.keep: The number of simulations to keep for posterior inference. Defaults to 1000
  
  # adapt: Indicates whether to adapt the values sigma.jump.mu & sigma.jump.p for the proposal generating distributions
  # sigma.jump.mu: The initial variance of the proposal generating distribution for sigma.mu
  # sigma.jump.p: The initial variance of the proposal generating distribution for sigma.p
  
  # contr.matrix: The matrix of contrasts required for posterior inference. If NULL, no logFC or delta.log.phi distribution will be generated
  
  # parameters.to.save: A vector naming the parameters required to be stored.
  
  # Check the design vectors
  if (!is.factor(conditions) || !is.factor(exons)) return(cat("Conditions & exons must be specified as factors","\n"))
  
  # Check that the parameters to save are contained in the model
  tot.param <- c("S","c.hi","mu.h","sigma.mu","p.k","sigma.p","q.hj","phi.hj","sigma.S")
  # Abort if any invalid parameters are requested to be saved
  if (length(setdiff(parameters.to.save,tot.param))!=0) stop(paste("Invalid parameter requested:", setdiff(parameters.to.save,tot.param),sep=" "))
  
  # Define the dimension parameters required. This could be done outside of this function.
  # Chips & Conditions
  H <- length(levels(conditions)) # A scalar, defining the number of conditions
  I <- length(conditions)         # A scalar, defining the total number of samples
  I.h <- vector("integer",H)      # Initialise the object I.h as a vector of length H. Contains the number of samples in each condition
  for (h in 1:H){
    I.h[h] <- length(which(conditions==levels(conditions)[h])) # Count the number of samples in each condition
  }
  # Exons & probes
  J <- length(levels(exons))  # A scalar defining the number of exons
  K <- length(exons)          # A scalar defining the number of probes
  K.j <- vector("integer",J)  # Initialse the object K.j as a vector of length J
  for (j in 1:J){
    K.j[j] <- length(which(exons==levels(exons)[j])) # Count the number of probes in each exon
  }
  # Collect them all together in a list for inclusion in the updateValues function
  indices <- list(H=H, I=I, I.h=I.h, J=J, K=K, K.j=K.j)
  
  # Form the model matrices & expand the BG prior into vector form
  model.matrices <- list(cond=model.matrix(~conditions-1), exons=model.matrix(~exons-1))
  B.prior$mean <- rep(B.prior$mean[1:indices$K],times=indices$I)
  B.prior$sd <- rep(B.prior$sd[1:indices$K], times=indices$I)
  
  # Now make sure the dimensions of the data match the dimensions specified by the exons & conditions vectors
  if (nrow(PM)!=K) return(cat("Incorrect specification of exons/probes","\n"))
  if (ncol(PM)!=I) return(cat("Incorrect specification of chips/conditions","\n"))
  
  # Configure which simulations to keep
  if(is.null(n.burnin)) n.burnin <- floor(n.iter/2)
  n.keep <- min(n.keep,n.iter-n.burnin) # Ensures that you can't keep more than actually exist
  keep <- round(seq(from=n.burnin+1,to=n.iter,length=n.keep)) # This gives the actual vector indexing the simulations to keep from each chain
  n.thin <- (n.iter-n.burnin)/n.keep # This is useful later
  
  # If inits are supplied, check that they are useable
  if (!is.null(inits) && !is.list(inits)){ # If inits are not supplied as a list
    return(cat("Inits must be supplied as a list","\n"))
  }
  
  if (!is.null(inits) && is.list(inits)) { # If inits are supplied, it must be as a list
    # Check the dimensions for each supplied parameter on each chain
    for (chain in 1:length(inits)){
      if (nrow(inits[[chain]]$S)!=K || ncol(inits[[chain]]$S)!=I) return(cat("Supplied inits for S are incorrect","\n"))
      if (length(inits[[chain]]$c.hi)!=I) return(cat("Supplied inits for c.hi are incorrect","\n"))
      if (nrow(inits[[chain]]$phi.hj)!=J || ncol(inits[[chain]]$phi.hj)!=H) return(cat("Supplied inits for phi.hj are incorrect","\n"))
      if (dim(inits[[chain]]$q.hj)[1]!=J || dim(inits[[chain]]$q.hj)[2]!=H || dim(inits[[chain]]$q.hj)[3]!=3 ) return(cat("Supplied inits for phi.hj are incorrect","\n"))
      if (length(inits[[chain]]$mu.h)!=H) return(cat("Supplied inits for mu.h are incorrect","\n"))
      if (length(inits[[chain]]$p.k)!=K) return(cat("Supplied inits for p.k are incorrect","\n"))
      if (length(inits[[chain]]$sigma.mu)!=1) return(cat("Supplied inits for sigma.mu are incorrect","\n"))
      if (length(inits[[chain]]$sigma.p)!=1) return(cat("Supplied inits for sigma.p are incorrect","\n"))
      if (length(inits[[chain]]$sigma.S)!=1) return(cat("Supplied inits for sigma.S are incorrect","\n"))
    }
    
    if (length(inits)<n.chains){ # If the inits are only supplied for some of the chains, generate the inits for the rest of the chains
      l <- length(inits)+1
      for (chain in l:chains) inits[[chain]] <- initialiseValues(PM=PM, indices=indices, conditions=conditions, exons=exons, model.matrices=model.matrices, B.prior=B.prior)
    }
    
  }
  
  if (is.null(inits)) { # If no inits have been supplied
    for (chain in 1:n.chains) {
      # Call the function initialise Values for each chain
      inits[[chain]] <- initialiseValues(PM=PM, indices=indices, conditions=conditions, exons=exons, model.matrices=model.matrices, B.prior=B.prior)
    }
  }
  
  # Initialise the objects to store the sims & the last.values. For computational efficiency, only save some parameters
  last.values <- vector("list",length=n.chains) # This holds the set values for each iteration
  sims <- vector("list",length=n.chains) # This is where all the kept iterations are stored
  if (!is.null(contr.matrix)) { # If contrasts are to be sampled as part of the process
    if (nrow(contr.matrix)!=H) stop("Invalid contrast matrix") # Abort the process if this is not specified correctly
    logFC <- vector("list",length=n.chains) # The samples of logFC
    delta.log.phi <- vector("list",length=n.chains) # The samples of delta.log.phi
  }
  else {
    logFC <- delta.log.phi <- c() # Create blank objects for easy inclusion at the output stage
  }
  
  # Now generate all the possible names for the sims object
  param.names <- vector("list")
  param.names$S <- paste(rep(paste("S[",1:K,sep=""),times=I),",",rep(1:I,each=K),"]",sep="")
  param.names$c.hi <- paste("c.hi[",1:I,"]",sep="")
  param.names$phi.hj <- paste(rep(paste("phi.hj[",1:H,",",sep=""),each=J),rep(paste(1:J,"]",sep=""),times=H),sep="")
  param.names$mu.h <- paste("mu.h[",1:H,"]",sep="")
  param.names$p.k <- paste("p.k[",1:K,"]",sep="")
  param.names$sigma.mu <- "sigma.mu"
  param.names$sigma.p <- "sigma.p"
  param.names$sigma.S <- "sigma.S"
  param.names$q.hj <- paste("q[",rep(paste(rep(1:J,times=H),rep(1:H,each=J),sep=","),times=3),",",rep(1:3,each=H*J),"]",sep="")
  sim.names <- as.vector(unlist(param.names[parameters.to.save])) # This is now the name of all the saved parameters
  # Generate the names for logFC & delta.log.phi
  
  # Initialse the objects for storing the data
  for (chain in 1:n.chains){
    
    last.values[[chain]] <- inits[[chain]] # Assign the inits to the last.values object
    if (!is.null(contr.matrix)) { # If the contrasts are to be sampled
      # Initialise a matrix with a row for every kept simulations & a column for every contrast
      logFC[[chain]] <- matrix(ncol=ncol(contr.matrix),nrow=length(keep))
      colnames(logFC[[chain]]) <- colnames(contr.matrix)
      # Do similar for delta.log.phi, but expand for every exon
      delta.log.phi[[chain]] <- matrix(ncol=ncol(contr.matrix)*J,nrow=length(keep))
      colnames(delta.log.phi[[chain]]) <- paste(rep(colnames(contr.matrix),each=J),"[",1:J,"]",sep="")
    }
    # Initialise the main sims matrix
    sims[[chain]] <- matrix(ncol=length(sim.names),nrow=length(keep))
    colnames(sims[[chain]]) <- sim.names
  }
  
  # Estimate f3.hj. This is independent of the chains as all input parameters are known
  f3.hj <- exp(t(t(log(f3(PM, indices, B.prior)))%*%model.matrices$exons)%*%model.matrices$cond)
  
  # Run the sampler in the adaptive phase. Once the acceptance rate for the previous 100 iterations is between 0.42 & 0.46, stop adapting that parameter
  # 0.44 is the optimal acceptance rate
  # This loop is essentially the part of the process that needs to be performed in C/C++ for efficiency
  temp.sigma.mu <- temp.sigma.p <- vector("list",n.chains) # These dummy objects hold all the sampled values for the adaptive phase
  mu.done <- FALSE # A control variable. Indicates whether adapting of sigma.jump.mu is finished
  p.done <- FALSE  # A control variable. Indicates whether adapting of sigma.jump.p is finished
  for (iter in 1:n.iter){
    
    for (chain in 1:n.chains) {
      last.values[[chain]] <- updateValues(PM=PM, old=last.values[[chain]], indices=indices, exons=exons, conditions=conditions, model.matrices=model.matrices, B.prior=B.prior, f3.hj=f3.hj, sigma.jump.mu=sigma.jump.mu, sigma.jump.p=sigma.jump.p) # Update all the parameters using the last values
      if (iter %in% keep){ # If the iteration is to be kept
        sims[[chain]][which(keep==iter),] <- unlist(last.values[[chain]][parameters.to.save]) # Add the last values to the correct row in the sims object
        if (!is.null(contr.matrix)) { # If the contrasts are to be sampled
          logFC[[chain]][which(keep==iter),] <- as.vector(t(contr.matrix)%*%last.values[[chain]]$mu.h) # Sample the logFC values
          delta.log.phi[[chain]][which(keep==iter),] <- as.vector(t(t(contr.matrix)%*%t(log(last.values[[chain]]$phi.hj)))) # Sample the delta.log.phi.values. This will give NA if phi.hj[x]==phi.hj[y]==c(0,1) & this can be corrected later.
          
        }
      }
      if (!mu.done) temp.sigma.mu[[chain]][iter] <- last.values[[chain]]$sigma.mu # Record the values until the adapting is finished
      if (!p.done) temp.sigma.p[[chain]][iter] <- last.values[[chain]]$sigma.p # Record the values until the adapting is finished
    }
    
    if (iter<=n.burnin){ # All adapting must be done during the burnin period
      
      if (!mu.done) { # If the adapting is still going for sigma.jump.mu
        sd.mu <- sd(unlist(temp.sigma.mu),na.rm=TRUE) # Find the current sd for the sampled values of mu
        if (sd.mu!=0 && !is.na(sd.mu)) sigma.jump.mu <- sd.mu*sqrt(2.4) # Only adapt if valid values for sd.mu are obtained
        if (iter >100) { # Check the acceptance rate per 100 samples, once there are 100 samples
          acc.mu <- length(which(matrix(unlist(temp.sigma.mu),ncol=n.chains)[(iter-100):(iter-1),]!=matrix(unlist(temp.sigma.mu),ncol=n.chains)[(iter-99):iter,]))/(n.chains*100)
          if (acc.mu<0.46 && acc.mu>0.42) mu.done=TRUE # Fix sigma.jump once the jumps are suitably efficient
        }
      }
      
      if (!p.done) { # If the adapting is still going for sigma.jump.p
        sd.p <- sd(unlist(temp.sigma.p),na.rm=TRUE) # Find the current sd for the sampled values of p
        if (sd.p!=0 && !is.na(sd.p)) sigma.jump.p <- sd.p*sqrt(2.4) # Only adapt if valid values for sd.p are obtained
        if (iter >100) { # Check the acceptance rate once there are 100 samples
          acc.p <- length(which(matrix(unlist(temp.sigma.p),ncol=n.chains)[(iter-100):(iter-1),]!=matrix(unlist(temp.sigma.p),ncol=n.chains)[(iter-99):iter,]))/(n.chains*100)
          if (acc.p<0.46 && acc.p>0.42) p.done=TRUE # Fix sigma.jump once the jumps are suitably efficient
        }
      }
      
    }
  }
  
  for (chain in 1:n.chains) {
    delta.log.phi[[chain]][which(is.na(delta.log.phi[[chain]]))] <- 0 # Change any NA values to zero. These can only occur if phi.hj[x]==phi.hj[y]==c(0,1)
  }
  
  # Return a list with the sims, the sampled values of logFC & delta.log.phi along with other input parameters
  list(sims=sims, logFC=logFC, delta.log.phi=delta.log.phi, n.chains=n.chains, n.iter=n.iter, n.burnin=n.burnin, contr.matrix=contr.matrix)
  
}
