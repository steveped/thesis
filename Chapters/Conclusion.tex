
\chapter{Conclusion} \label{chap:FinalDiscuss}

\fancyhead{}
\fancyhead[RO,LE]{Chapter \ref{chap:FinalDiscuss}. \emph{Discussion}}
\fancyfoot{}
\fancyfoot[LE,RO]{\thepage}

\clearpage
\section{The Wider Context}\label{sec:FinalDiscuss}

By the arrival of Whole Transcript (WT) Arrays as a research platform, detection of differentially expressed genes via 3' arrays was a relatively mature field.
Platforms such as Exon Arrays promised to target more genes and by capturing information across the length of the gene, information about transcript-specific expression seemed viable.
However, as seen in this study, reliably obtaining this level of information presented many challenges.\\
\\
The difficulties underlying transcript identification is not restricted to WT Arrays, but has also been a significant challenge for RNA-Seq analysis.
Only recently have approaches such as \texttt{kallisto}\spCite{pmid27043002} begun to produce consistent results, although issues such as RNA degradation, GC-bias\spCite{LoveGCBias}, length-bias, sequencing depth and complex expression patterns still leave the most confident calls to more highly expressed genes\spCite{ZhangRNASeq2017}.
RNA-Seq brings the advantage of direct target measurement from the obtained data, as opposed to array technology which measures both direct target and off-target molecules via non-specific binding, and is largely restricted to predefined gene models.
RNA-Seq also enables use of tools such as StringTie\spCite{PerteaStringtie} for construction of previously unidentified transcripts, however these are also dependent on the choice of aligners and their ability to produce accurate alignments, particularly across gene families with high levels of homology.
Moreover, the meaning of differential expression itself, and how this is measured, has been under review since the advent of these technologies\spCite{pmid22383036}.\\
\\
At the commencement of this body of work, these techniques and tools were a long way off, and RNA-Seq itself was still prohibitively expensive for many research groups.
This left smaller groups eagerly producing data via array technology, in the hope of producing high-quality results.
Whilst this was viable at the gene-level, detection of AS events using array data proved to be a significant challenge, which this analysis potentially addresses in a greater depth than any previous work.
The vast body of WT array data which exists in public data repositories such as GEO\spCite{GEO} and Array Express\spCite{ArrayExpress2015} remains a resource which is still largely untapped at the exon and transcript level.
%The BMEA model offers an opportunity for a degree of careful exploitation of this resource.

\section{Key Findings}\label{sec:KeyFindings}

\subsection{Background Signal}\label{subsec:BGSignal}

Beyond the development of a full Bayesian model for detection of AS events, this body of work contains some important additional observations.
The first of these is the discrepancy between the fitted model coefficients for GCRMA across multiple datasets, and those calculated using 3' arrays (Figure \ref{fig:GCRMAcoefs}).
RMA remains the preferred background correction strategy for WT arrays due primarily to difficulties in the implementation of GCRMA using \texttt{aroma.affymetrix}.
This may have been fortuitous for researchers as na\"ive use of GCRMA would be inappropriate for BG correction of WT arrays due to the clear differences in probe behaviour.
The distinct characteristics of AG and MM probes included on Exon Arrays further complicate this picture in comparison to 3' arrays.\\
\\
The strategy proposed under BMEA is essentially agnostic to the choice of MAT or GCRMA for estimation of bin-specific values $\hat{\lambda}_l$ and $\hat{\delta}_l$, with this model only playing a role in assignment of probes to a bin.
The parameter estimates themselves derive from the \textit{observed values} for BG probes within each bin, with these values showing minimal difference between bins defined under either the MAT or GCRMA model (Figure \ref{fig:allObsAG}).
The MAT model was chosen for the majority of analysis as the model itself better captures the true nature of data from BG probes than GCRMA (Figure \ref{fig:agFitVsObs}).\\
\\
%The choice of AG or MM probes for obtaining the parameters $\lambda_l$ and $\delta_l$ presented a subtle but fairly simple choice.
%The set of MM probes included on Exon Arrays were designed to match PM probes which target genes not expected to be abundantly expressed, containing only one sequence change from these PM probes.
%The AG probes are much further removed from known expressed sequences, and as such will be less likely to be biased towards any specific ``near-miss" sequences.
%The consequences of this choice will be that parameters estimated using MM probes will contain an upwards bias, ensuring a larger number of genes and exons will be excluded using $Z$-score filtering, and providing a downward bias to estimates of expression and possibly also for exon inclusion rates.\\
A strong advantage of the approach taken under BMEA was that the previously unidentified disparity in NSB behaviour between samples was able to be clearly identified as a confounding effect which is technical, not biological in nature (Section \ref{sec:UnevenNSB}).
The comparison between quartiles of BG and PM probes (Figure \ref{fig:AgPmQuantiles}) provides a simple QC check for any similar behaviour within datasets under analysis, and guidelines as to the interpretation of any exon-level predictions obtained.
Whilst not formally investigated, the observed skew in the distribution of $\tilde{t}$-statistics in Figure \ref{fig:TstatBias} involved both comparisons against stimulated \Treg, and this detected effect is likely to have played a role in this behaviour.
The approach taken to rectify this in Section \ref{subsec:LinearMod} remains an appropriate remedy to this problem.
In addition, this artefact may have been a strong contributor to the disparate behaviours in Section \ref{sec:ExonMethods}, especially considering that the overwhelming majority of candidate AS events detected under FIRMA were sourced from a comparison involving stimulated \Treg.\\
\\
Although not raised in previous sections, a clear alternative strategy for assigning the values $\lambda_{hijk}$ and $\delta_{hijk}$ would have been to use a sliding window approach and using the values obtained for some number (e.g. $n=1000$) of probes which were the closest in value to the PM probe of interest.
The use of bins provided a simple and clear way to identify these NSB artefacts, which may have remained more difficult to detect under an alternative approach.
Finally, the assignment of PM probes to bins based on expected BG signal, provided a simple methodology for calculation of $Z$-scores for both whole-gene and exon-level probesets.
Whilst initially conceived as a test for detection of true signal above background (DABG) and exclusion of probesets which only contain \textit{off-target} signal, these scores provided measures for the overall strength of the \textit{on-target} signal component over background.
These scores proved to be an invaluable tool in assessment of suitable candidates for differential transcript usage, and restriction to probesets for which $S\gg B$ strongly improved the accuracy of predicted AS events.\\
\\
Beyond analysis under BMEA, the use of $Z$-scores is possible for exclusion of genes which are not considered as expressed.
Even when performing a gene-level analysis using RMA, the removal of these genes will clearly limit the number of false positives and increase the power of an experiment from the perspective of multiple testing considerations.
When performing downstream analysis, such as GO or TFBS enrichment, this enables clear definition of a set of genes considered as expressed with the tissue of interest, and can provide a more suitable set of background genes than just the wider genome.
If using the wider genome as the background or control dataset, terms which simply define the cell or tissue of experimental interest will commonly appear in lists of results, as opposed to terms associated with \textit{the specific treatment within} that tissue.
By using a set of reference genes which are specifically expressed within the cell type of interest, many of these terms which simply describe the cell type will not be included as enriched and a more informative set of results can be obtained.

\subsection{The BMEA Model}\label{subsec:DiscussBMEA}

After completion of much development and detection of confounding factors, the BMEA successfully identified an important biological process for which alternate transcript usage appears to play a role (Section \ref{subsec:bmeaBiologicalImplications}).
The immune synapse itself has been implicated in the exchange of signalling molecules between cells\spCite{pmid21505438}\spCite{pmid24487619}, and as such the identification of non-coding transcripts which center around this process may be of much interest.
Importantly, one of these transcripts was validated with qPCR (\textit{CD53 - ENST00000471220}), confirming that at least one ncRNA associated with this process is indeed differentially regulated in response to activation.
However, current association of this gene and process has been performed primarily at the protein level\spCite{Draber15112011} and association of this transcript with the same process is still speculative at this point.
Although being far from a definitive description of associated processes, this work has laid the foundation for potentially exciting research.\\
\\
The majority of candidate AS events detected under BMEA are associated with the common activation signature, and in general appear to be a relatively small number of events compared to the levels of differential expression observed at the gene level.
Three important factors are likely to have played a role in this: 1) The observed artefact for stimulated \Treg samples (Section \ref{subsec:batchNSB}); 2) Lower sample numbers in the Resting \Treg Vs \Th comparison; and, 3) The underlying biology.\\
\\
The background signal artefact observed in stimulated \Treg samples in this dataset, is a type of technical effect not previously characterised and is difficult to easily ascribe to a protocol variation, beyond the general hybridisation and washing steps.
The fact that a similar effect was found in the Tissue Mixture dataset suggests that this type of effect may be more common than previously realised, and does account for some of the anecdotal difficulty associated with AS detection using Exon Arrays, and the ease with which RNA-seq rendered the technology relatively obsolete.
In particular, this effect largely explains the unsatisfactory FIRMA results in Section \ref{subsec:FIRMAAnalysis}, confirming that many of these were likely to be false positives.
The characterisation of this effect allowed for exclusion of candidate AS events which exhibited the predicted behaviour, however this directly reduced the predictive power of this dataset by effectively excluding two comparisons.
For candidate AS events detected in other comparisons, the ability of stimulated \Treg to act as the second, confirmatory comparison was also significantly reduced.
The confirmation of alternate transcript usage for \textit{CD53} as a common activation effect, rather than the predicted \Th-specific activation response reinforces this, and it is entirely possible that many of the predicted AS events from other comparisons may not be correctly classified as resting \Treg or stimulated \Th effects only.\\
\\
The four-way layout of the \Treg dataset, and the changing mixture levels made identification of this type of effect simple to detect.
In a more simple, but common experimental design, with only two cell types, this may prove more of a challenge.
There will only be two conditions for each bin, and identifying whether one cell type has an positive or negative bias for estimation of $\lambda_{il}$ will be less clear.
A clear alternative may be to average across an experiment to arrive at values for $\lambda_{\cdot l}$ and $\delta_{\cdot l}$, however as noted in Figure \ref{fig:postQNDensities} this impact may also be evident within the overall set of intensities, including the PM intensities.
If they are also impacted, this may potentially be a difficult effect to resolve beyond taking a cautious and careful approach to interpretation of results.\\
\\
When selecting candidate AS events, the restriction on comparisons involving stimulated \Treg clearly reduced the power of this dataset, with only the resting \Treg Vs \Th ($n=4$) and stimulated  vs resting \Th ($n=5$) comparisons being able to offer their full capability.
It is also of key importance that the initial power calculations for this dataset (Section \ref{subsec:PowerCalcs}) were performed by estimating fold-change under the common model assumptions used by RMA/PLM.
The detection of AS events is a fundamentally different question, and as such would require a different set of calculations.
As minimal information regarding exon-level analysis was available before analysis began, this dataset is likely to be under-powered for this level of analysis.
The far smaller numbers of probes utilised in exon-level probesets is likely to lead to greater instability of estimates than at the gene-level, where far greater probe numbers are able to be used to provide more stable estimates of gene-level terms.
This smaller number of probes within an exon-level probeset also gives less opportunity for the data to overcome any influence the priors have on final results.
The alternate specifications used for generation of the custom CDF may have slightly enabled an improved performance by creating at least a limited number of probesets with increased probe numbers.\\
\\
The Tissue Mixture dataset was able to detect hundreds of AS events from 3 samples in each of the 100\% tissue groups, lending confidence to the capacity of the BMEA approach under low sample numbers.
How many of the true AS events this represents remains a difficult question to answer, and with these being two highly differentiated tissues, it is likely that detected events represent only a small proportion of the true AS events which exist between the tissues.
Comparisons between the four cell types in the \Treg dataset involve four very similar cell types, and as such, a far lower number of AS events may be expected than in the Heart Vs Brain comparison.
Thus, given the underlying biological similarities, a far smaller pool of true AS events would have been present in the initial samples, and the much lower numbers of candidate AS events was not unexpected.\\
\\
In testing using simulated data BMEA appeared to strongly outperform the FIRMA approach and it is not unreasonable to expect this behaviour to continue when working with true experimental data.
The addition of filtering steps during selection of candidate AS events under BMEA makes a direct comparison of approaches difficult, as development of similar strategies for analysis using FIRMA was not of primary interest.
However, a key advantage of BMEA remains the inclusion of intuitive model terms, where the proportion of transcripts containing each exon is directly modelled, making results far easier to interpret and communicate with researchers.
The simpler interpretation of Bayesian results when compared to the conventional frequentist approach of rejecting or accepting $H_0$, additionally makes interpretation by researchers an easier task.

\section{Future Directions}

\subsection{The BMEA Model}

Whilst successfully providing strong leads for future experiments, BMEA has much scope for further improvements, notwithstanding the wider shift towards sequencing technologies.
The \Treg dataset contained matched samples from within the same donor, and the model detailed under BMEA does not directly take this data structure into account.
This is clearly an area which could be further explored by the definition of a model in which the change in exon-inclusion rates ($\Delta\phi_{hj}$) and overall fold-change ($\Delta \mu_h$) are defined in this manner.
This may have additional advantages for specifying other nesting structures within sample groups such as sibships for murine experiments or other similar experimental designs.\\
\\
The exon-level term $\phi_{hj}$ was specifically defined to sidestep difficulties defining the distribution of this term across samples within a cell-type.
Conceptually, this does allow for detection of candidate AS events which are not consistent within each cell type, however the general frequentist approach of detection of average differences would behave in a similar manner.
Incorporation of a sample-specific term at the exon level, with a relationship to $\phi_{hj}$ in keeping with the structure of chip-effects ($c_{hi}$) and the cell-type specific expression level ($\mu_h$), would be a further area of potential future development.\\
\\
The assignment of sample-weights has become a common-place procedure when analysing 3' array data and for RNA-seq data when utilising the \textit{voom} method\spCite{Law2014}.
Considering the observations regarding uneven hybridisation, the development of a model incorporating sample weights may be an additional development which strengthens this approach.\\
\\
A key refinement to the model which would be the clear next step would be to develop an approach which better models normalisation.
All analyses used quantile normalised data, as that appeared to be the most appropriate for gene-level results.
The appropriateness of this for exon-level analysis is less clear, as the fundamental requirement for equal amounts of total hybridised sample is no longer relevant.
A superior approach may be to normalise data by the incorporation of an additional term in the model, as is used by approaches such as RUV\spCite{RUV2012}.
This may better allow for accurate assessment at the gene-level, without compromising the power of the approach to detect changes at the exon-level, as noted in Section \ref{subsec:QN4BMEA}.
Additionally finding a method to more satisfactorily manage any unexpected bias in $\hat{\lambda}_{il}$ may also prove strongly advantageous.\\
\\
Whilst Figure \ref{fig:allObsAG} showed distributions which were a reasonable approximation of normally distributed data, the assumption of normality for the background signal, is also an area which may require deeper exploration.
At each iteration $t$ of the MCMC process, BMEA samples $\hat{S}^t = PM - \hat{B}^t$, with $\hat{B}^t$ drawn from the prior $\mathcal{N}(\lambda, \delta)$.
An alternative may be to sample observed values from AG or MM probes with similar properties to each PM probe, or to construct an empirical distribution based on observed values.
However, neither of these approaches would correctly deal with competitive binding of target and off-target sequences.\\
\\
The correlations between replicates for values such as $\mu_h$ also showed an increase in variability for genes at the low end of expression values which is reminiscent of that for replicates under MAS5.0\spCite{ZakharkinSources2005}, although this was not as extreme as has been noted under MAS5.0.
BMEA essentially introduces variance for BG signal and variance for true signal, as does MAS5.0 and this is to be expected to a certain extent.
An alternative model-based approach for $E(S \vert PM)$ in this context may be a further avenue to explore to better manage this.
In particular, the representation of BG signal offered by MAT may prove advantageous and methods of incorporating this into a model-based approach such as for GC-RMA may be feasible.\\
\\
An unplanned development which arose from this work was the definition of an alternate CDF, which  was able to partially overcome the limitation of $\leq4$ probes per exon-level probeset.
The definitions used in Section \ref{subsec:newCDF} utilised the common probes for Ensembl-based genes and exons.
Probes which mapped to common sets of exons were considered as probesets, to allow for the potential detection of novel AS events.
Considering that no novel AS events were detected in Section \ref{sec:finalBMEATreg}, an alternative may have been to define probesets based on probes which target common transcripts.
If using this approach, the annotation file could then be defined to contain the appropriate transcript identifiers associated with the probesets considered as detecting a putative AS event.
This would effectively render them less as exon-level probesets, more as alternate-transcript detecting probesets, and these probesets would generally be far larger than 4 probes.
Whilst it is expected that these may provide more stable estimates at both the gene and alternate-transcript level, this would limit the capacity of an experiment to detect novel AS events.
Interestingly, this would contradict the implied design of ``Exon" Arrays, which may subtly bias the researcher into approaching these analyses by considering the exon as the important unit, as opposed to the true goal of detecting alternate transcript usage.\\
\\
The CDF defined in Section \ref{subsec:newCDF} was also based on the Ensembl gene models which contain numerous predicted transcripts with minimal supporting evidence.
Whilst this proved advantageous for the detection of many of the events in Section \ref{subsec:finalDetASTreg}, this may also increase the number of false positives due to the presence of many small probesets targeting subtle changes in exon definitions.
A more conservative approach may be to define transcripts or exon-level probesets using more stringent gene models such as those contained in the RefSeq database.
As these are generally better characterised transcripts than those in the Ensembl database, this may provide an additional stringency on the biological relevance of any candidate alternate transcript usage.\\
\\
Whilst Exon Array usage is in clear decline, thousands of experiments using this technology already reside in the GEO database (\url{https://www.ncbi.nlm.nih.gov/geo/}).
As discussed above, the ability to redefine the CDF used for analysis also opens the possibility of considering Affymetrix Gene Arrays as candidates for analysis using BMEA, as these also contain probes targeting the length of each gene.
Thousands of these additional datasets exist in public repositories.
Whilst many of these datasets will suffer from a lack of experimental power, the development of BMEA will enable a degree of retrospective mining of these datasets, and the exploitation of an existing resource has thus been enabled to a far greater extent than previously.

\subsection{T Cell Research}

Surprisingly, the majority of the detected AS events tracked more closely with activation than with differences between \Treg and \Th cells.
Whether this is an artefact of the limited power, or representative of the underlying biology is unclear, however this research has shed important light on an important area of T cell biology.
Much of the \Treg-APC communication process is still poorly understood, and this research may provide insights into key genes involved in this process.
Further study into all isoforms of interest is very much a blank slate for biologists as most of the predicted and experimentally verified transcripts detected here are still poorly understood.
The surprising number of non-coding transcripts detected as differentially expressed provides an exciting additional opportunity for further investigation in this rapidly growing area of biological research.

% Add some comments on FOXP3...