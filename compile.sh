#!/bin/bash

if [ -f "PedersonThesis.aux" ]
 then rm "PedersonThesis.aux"
fi

if [ -f "PedersonThesis.bbl" ]
 then rm "PedersonThesis.bbl"
fi

if [ -f "PedersonThesis.blg" ]
 then rm "PedersonThesis.blg"
fi

if [ -f "PedersonThesis.lof" ]
 then rm "PedersonThesis.lof"
fi

if [ -f "PedersonThesis.lot" ]
 then rm "PedersonThesis.lot"
fi

if [ -f "PedersonThesis.out" ]
 then rm "PedersonThesis.out"
fi

if [ -f "PedersonThesis.synctex.gz" ]
 then rm  "PedersonThesis.synctex.gz"
fi

pdflatex PedersonThesis.tex
bibtex PedersonThesis
pdflatex PedersonThesis.tex
pdflatex PedersonThesis.tex
