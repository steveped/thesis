\chapter{BMEA Package Design and Performance} \label{app:RPackage} 

\fancyhead{}
\fancyhead[RO,LE]{Appendix \ref{app:RPackage} \emph{BMEA Package Design and Performance}}
\fancyfoot{}
\fancyfoot[LE,RO]{\thepage}

\section{Package Design}

\subsection{The MCMC Process}\label{subsec:MCMCProc}
The MCMC process itself was written in the language C, leaving R as the user interface via the package \textit{BMEA}, giving an $\sim 8$-fold improvement in computational time when compared to  the pure R code of Section \ref{subsec:SimS}.
Instead of running separate chains in parallel, the package was written to break the dataset into batches of genes, with each batch being fitted in parallel.
All parameter updates were as defined in Appendix \ref{app:Posterior}.

\subsection{Aroma Affymetrix Conventions}\label{subsec:aromaConventions}
As the BMEA process is based around generating posterior distributions for the parameters of interest, this equates to a huge amount of data.
Given the number of genes is in the tens of thousands, and the number of parameters fitted for a given gene will often be $>100$ with many thousands of sampled values during the MCMC process, great care was required to determine what should be retained in the output of the process. \\
\\
The architecture of the \textit{aroma.affymetrix} package\spCite{BengtssonAroma2008} was taken as the basis for \textit{BMEA} and extended to store the required information.
This package uses a rigid directory structure, with a minimal directory structure shown in Figure \ref{fig:AromaAffyDir}.
Under this structure, the CDF file for HuEx-1_0-st-v2 arrays is placed in the \path{annotationData/chipTypes/HuEx-1_0-st-v2} folder, and so on for every chip type under investigation.
The raw data, in the form of .CEL files, is likewise stored in the folder \path{rawData/experimentName/HuEx-1_0-st-v2}, where \path{experimentName} is the working name of the current experiment.
The name of the chip-type (HuEx-1_0-st-v2) can also be changed, but must match the name of the supplied CDF exactly.\\

\begin{figure}[!t]
	%\centering
	\dirtree{%
	.1 \textit{parentDirectory}.
	.2 annotationData.
	.3 chipTypes.
	.4 HuEx-1_0-st-v2.
	.5 HuEx-1_0-st-v2.CDF.
	.2 rawData.
	.3 \textit{experimentName}.
	.4 HuEx-1_0-st-v2.
	.5 \textit{myData1.CEL, myData2.CEL etc.}.
	}
	\caption[Minimal initial directory structure required to begin an analysis using the package aroma.affymetrix.]{Minimal initial directory structure required to begin an analysis using the package \texttt{aroma.affymetrix}, with data generated on Human Exon 1.0 ST arrays.
	Directory and file names given in italics are able to be changed as required for an individual analysis, with the remaining paths being the rigid directory structure required.}
	\label{fig:AromaAffyDir}
\end{figure}

In addition to this directory structure, a reduced .CEL file type is used by this package, referred to as a \textit{monocell} .CEL file.
A corresponding monocell CDF is generated in the initial stages of an analysis, and written to the folder \path{annotationData/chipTypes/HuEx-1_0-st-v2}.
This file structure allows only one entry (i.e. cell) per exon, making it very efficient for writing summary values at the probeset (i.e. gene or exon) level, and giving a much smaller file size than the full .CEL file structure, which contains probe-level information.
This file format is used by \textit{aroma.affymetrix} to write gene-level expression estimates to disk using the directory structure \path{plmData/exptName/HuEx-1_0-st-v2}.\\
\\
These structures and file-types are stored in R as objects using the \texttt{S3} class \texttt{AffymetrixCelSet}, which is used to read information from a collection of .CEL files into R.
As array analysis often involves multiple processing steps, the use of \textit{tags} is utilised by \textit{aroma.affymetrix} by the addition of a tag after a comma as the directory \path{exptName,tag}, to denote any steps that have been performed on the .CEL files in the lower-level directories.\\


\subsection{BMEA Extensions to Aroma Affymetrix Structures}\label{subsec:BmeaExtendsAroma}
The above conventions were extended in BMEA in order to store values for background priors, and the key values from posterior distributions.
Values for the Background Signal priors were treated in the same manner as the raw data files, and were stored in a probe-specific manner, using .CEL files matching the original ``rawData" directory structure, using the directories named \path{backgroundPriors/experimentName,lambda} and \path{backgroundPriors/experimentName,delta/} (Figure \ref{fig:bmeaDirStructure}).
All values were estimated in an array specific manner as described in Section \ref{subsec:3ApproachesForB}, effectively allowing the notation $\lambda_{hijk}$ and $\delta_{hijk}$, instead of requiring the additional indexing subscript $l$ to indicate which bin the probe belongs to.
As the location ($\lambda$) and scale ($\delta$) parameters were able to be defined as separate \texttt{AffymetrixCelSet} objects, the new informal \texttt{S3} class \texttt{AffymetrixCelSetList} was introduced to collect these related parameter files into single objects within the R environment.\\
\\
For posterior distributions, all values are written to the directory \path{bmeaData}, with two subdirectories \path{bmeaData/modelData} and \path{bmeaData/contrastData}.
Instead of saving all MCMC sampled values, by default only the $0.025, 0.25, 0.5, 0.75$ and $0.975$ quantiles were saved, along with the posterior mean and the convergence statistic $\hat{r}$\spCite{GelmanBayesian2004}, for any model parameter chosen for saving (Figure \ref{fig:bmeaDirStructure}).
All values for a saved parameter (e.g. $\mu$) were written to the directory \path{bmeaData/modelData/experimentName,parameterName}, again using the tag system of naming directories.
The same sets of summary statistics for any contrasts were written to the directories \path{bmeaData/contrastData/experimentName,logFC} and \path{bmeaData/contrastData/experimentName,phiLogFC}, with the addition of the $B$-statistic.
In all cases, the \texttt{AffymetrixCelSetList} object type was used to manage the relationship between the directory structure and the R environment.\\
\\
For chip, condition, exon or any other grouped term, \textit{monocell} .CEL files are used to store the summary statistics, with only probe-level values requiring the full .CEL file structure.
All model parameters are able to be saved, with only the chip effects ($c_i$), condition-specific expression levels ($\mu_h$), exon proportions $\phi_{hj}$ saved by default, in the interests of minimising disk storage, and minimising disk writing times whilst the process was running.
Both logFC ($\Delta \mu$) and phiLogFC ($\Delta \log \phi_j$) are also saved by default.\\

\begin{figure}[p]
	%\centering
	\small
	\dirtree{%
	.1 \textit{parentDirectory}.
	.2 annotationData.
	.3 chipTypes.
	.4 \textit{cdfName}.
	.5 \textit{cdfName.CDF}.
	.2 rawData.
	.3 \textit{experimentName}.
	.4 cdfName.
	.5 \textit{myData1.CEL, myData2.CEL, $\ldots$ etc}.
	.2 backgroundPriors.
	.3 experimentName,lambda.
	.4 cdfName.
	.5 myData1.CEL, myData2.CEL $\ldots$ etc.
	.3 experimentName,delta.
	.4 cdfName.
	.5 myData1.CEL, myData2.CEL $\ldots$ etc.
	.2 bmeaData.
	.3 modelData.
	.4 experimentName,c.
	.5 cdfName.
	.6 myData1,2.5\%.CEL, myData1,25\%.CEL, myData1,50\%.CEL, myData1,75\%.CEL, myData1,97.5\%.CEL, myData1,mean\%.CEL, myData1,sd\%.CEL, myData1,rHat.CEL, myData2,2.5\%.CEL etc..
	.4 experimentName,mu.
	.5 cdfName.
	.6 \textit{group1,2.5\%.CEL, group1,25\%.CEL, $\ldots$ etc}.
	.4 experimentName,phi.
	.5 cdfName.
	.6 \textit{group1,2.5\%.CEL, group1,25\%.CEL, $\ldots$ etc}.
	.3 contrastData.
	.4 experimentName,logFC.
	.5 cdfName.
	.6 \textit{cont1,2.5\%.CEL, cont1,25\%.CEL, $\ldots$ etc}.
	.4 experimentName,phiLogFC.
	.5 cdfName.
	.6 \textit{cont1,2.5\%.CEL, cont1,25\%.CEL, $\ldots$ etc}.
	}
	\caption[Example directory structure as generated by BMEA after specification of the minimal structure from Figure \ref{fig:AromaAffyDir}.]{Example directory structure as generated by BMEA after specification of the minimal structure from Figure \ref{fig:AromaAffyDir}. 
	Names shown in italics are set by the user, with group and contrast names defined within R and propagated through all directories.
	The experiment name, CEL file names and the CDF name are sourced from the original data structure and are also propagated through all directories.}
	\label{fig:bmeaDirStructure}
\end{figure}


\section{Algorithm Performance}\label{sec:AlgoPerf}

The recovery of simulated parameters from Chapter \ref{chap:BuildingR} is given in the following sections, with assessment of the technical performance of the package, in terms of computational time and convergence statistics given in Section \ref{sec:TechPerformance}.

\subsection{Recovery of Simulation Parameters}\label{subsec:ParamRecovery}

All fitted values for $\mu_h$, $\phi_{hj}$, $\sigma_{\mu}$, $\sigma_p$ and $\sigma_S$ were compared to the true values set during generation of the simulated data (Table \ref{tab:corAndBias}).
Posterior means were used as point estimates for $\mu_h$ with posterior medians being used for all other parameters.
Performance was assessed for both BMEA and BMEA-Z, with the latter utilising $Z_g$ and $Z_j$ to remove undetectable genes and exons respectively.
Pearson correlations were calculated, and bias was found as defined by 
\begin{align*}
\text{Bias}[\hat{\theta}] = E_{x \vert \theta}[\hat{\theta} - \theta]\,.
\end{align*}

Comparison between the BMEA and BMEA-Z models revealed no specific trend of higher or lower correlations with the inclusion of the $Z$-score steps (Table \ref{tab:corAndBias}).
However, with the inclusion of $Z$-scores the bias was slightly greater for all parameters except $\sigma_S$.

\begin{table}[!hb]
	\caption[Comparison between fitted values under BMEA and simulated values for continuous variables.]{
	Comparison between fitted values under BMEA and simulated values for continuous variables. 
	Fitted values for the mean expression-level ($\mu_h$) were taken as the posterior means, whilst fitted values for variance terms were taken as the posterior medians.
	Correlations between fitted and simulated values are shown ($\rho$), with the bias provided as $E_{x \vert \theta}[\hat{\theta} - \theta]$.
	Positive values for bias indicate that fitted values are larger on average than the true values, with the converse being true for negative values.}
	\centering
	\begin{tabular*}{0.9\textwidth}{@{\extracolsep{\fill}}+l^l^r^r}
	 	\toprule
	 	\rowstyle{\bfseries}
		Parameter & Model & $\mathbf{\rho}$ & Bias[$\mathbf{\hat{\theta}}$] \\ 
	  	\midrule
		  \multirow{2}{*}{$\mu_{h}$} & BMEA   & 0.922 & 1.121\\ 
		  						     & BMEA-Z & 0.944 & 1.277 \\
		  						     
        \midrule		  
		  \multirow{2}{*}{$\phi_{hj}$} & BMEA   & 0.719 & -0.313 \\ 
		  							   & BMEA-Z & 0.708 & -0.368 \\ 		  						     
  	  	\midrule
		  \multirow{2}{*}{$\sigma_{\mu}$} & BMEA   & 0.453 & 0.121 \\ 
		  								  & BMEA-Z & 0.457 & 0.133 \\ 
	  	\midrule		  
		  \multirow{2}{*}{$\sigma_{p}$} & BMEA   & 0.625 & -0.372 \\ 
		  							    & BMEA-Z & 0.672 & -0.417 \\ 
	  	\midrule		  
		  \multirow{2}{*}{$\sigma_{S}$} & BMEA   & 0.738 & -0.148 \\ 
		  							    & BMEA-Z & 0.765 & -0.139 \\ 
	   	\bottomrule
	\end{tabular*}
	\label{tab:corAndBias}
\end{table}


\subsubsection*{Expression Levels}
Fitted Vs Simulated values for $\mu_h$ are shown in Figure \ref{fig:muFitted}.
Across all patterns and simulated cell-types, simulated and fitted values for expression levels showed a high degree of concordance ($0.922 < \rho < 0.944$; Table \ref{tab:corAndBias}).
Notably, without the use of $Z$-scores, simulations using the splicing pattern with 40\% of probes containing no true signal (Pattern 3) showed a clear downward bias in expression estimates.
This was not particularly unexpected, as the expression level would have effectively been estimated as $\sim 0.6\mu$ for these data points.
The alternate model incorporating $Z$-score filtering of exons showed none of this bias.
Taking posterior means as representative of point estimates, the remainder of simulated genes showed an overall positive bias for $\mu_h$ which was most evident at the low end of the expression range.\\


\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{Appendix/muFitted.png}
	\caption[Fitted values for $\mu$ compared to simulated values.]{Fitted values for $\mu$ compared to simulated values. 
			Posterior means were used as point estimates for fitted values.
			Genes omitted using $Z$-scores prior to fitting are not shown.
			The dashed line represents the line $y = x$.
			Simulations with splicing pattern 3 are coloured red.}
	\label{fig:muFitted}
\end{figure}

\FloatBarrier

\subsubsection*{Variance Components}
The three variance components of the model ($\sigma_{\mu}, \sigma_p$ \& $\sigma_S$) showed more variable results than for expression levels (Table \ref{tab:corAndBias}).
For variance between samples within an experimental condition ($\sigma_{\mu}$) fitted values varied widely around the simulated values by a factor of up to 5-fold, showing a generally upwards bias in the fitted values, but with a positive correlation between the two sets of values (Table \ref{tab:corAndBias}).\\
\\
Correlations between simulated values and posterior medians were higher for probe-level variance ($\sigma_p$) than for $\sigma_{\mu}$.
A general downwards bias was also noted, with the strongest downwards bias evident amongst those data points with the lowest expression values (Figure \ref{fig:sigmaFitted}).
Of the variance terms, the highest correlations between fitted and simulated values were found for the overall signal-level variance term ($\sigma_S$).
Once again, an overall downwards bias was evident for the fitted values, with this being the most pronounced for data points with the lowest expression values.\\

\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{Appendix/sigmaFitted.png}
	\caption[Fitted values for $\sigma_S$, $\sigma_p$ and $\sigma_{\mu}$ compared to simulated values.]{Fitted values for $\sigma_S$, $\sigma_p$ and $\sigma_{\mu}$ compared to simulated values.
			Posterior medians were used as point estimates for fitted values, with correlations given in Table \ref{tab:corAndBias}.
			Points are coloured based on the mean simulated expression levels ($\bar{\mu}_h$).
			Axes are shown on the $\log_{10}$ scale. 
			The dashed line represents the line $y=x$.
			Genes omitted using $Z_g$-scores prior to fitting are not shown.}
	\label{fig:sigmaFitted}
\end{figure}

\subsubsection*{Exon Proportions}

Comparison of fitted values for $\phi_{hj}$ to simulated values was performed again using posterior median values as point estimates.
For splicing pattern 1, in which all exons were consistently included, the IQR for point estimates was $0.51 < \phi_{hj} < 0.68$ for BMEA and $0.49 < \phi_{hj} < 0.68$ for the model including $Z$-scores (Figure \ref{fig:phiFitted1to5}).
These values were considerably below the simulated values of $\phi_{hj} = 1$ and likely show the influence of the prior $\phi \sim \mathcal{U}(0, 1)$, especially considering only 4 probes were simulated for each exon.
This also largely explains the inflated estimates of $\mu_h$ noted previously.
Across the remainder of the splicing patterns, the range of values for each exon generally tracked the simulated values very closely, with the model incorporating $Z_j$ scores showing a clear influence of removed exons by setting $\phi_{hj} = 0$ for point estimates.\\
\\
A clear exception to this was Pattern 3, in which the unfiltered BMEA analysis incorrectly fitted Exons 1 to 4 with values for $\phi_{hj} > 0$.
The IQR for these fitted values across all simulations was $0.25 < \phi_{hj} < 0.46$, which was considerably lower than the included exons, but still far above the true value of $\phi_{hj} = 0$, which gives a clear explanation for the behaviour seen in Figure \ref{fig:muFitted}.\\
\\
For patterns in which cell-type B contained a shorter transcript, patterns with a single or double exon skip (Patterns 8 and 9) showed fitted values very much in keeping with expected behaviours (Figure \ref{fig:phiFitted6to10}).
However a surprising artefact was noted in cell-type A for exons simulated as missing in cell-type B.
In the cell-type for which these exons were included, the IQR for fitted values was higher ($0.59 < \phi_{hj} < 0.75$) using BMEA without $Z_j$-score filtering than for the consistently included exons.
Similar, but more exaggerated behaviour was noted in Pattern 10, where higher point estimates for $\phi_{hj}$ were observed in cell-type A for exons simulated as missing in cell-type B.
However, for exons included in both cell types for this pattern (Exons 1 to 5, 10), point estimates for $\phi_{hj}$ were lower in cell-type A than for cell-type B.
This splicing pattern was also noted as generating considerable false positives for logFC under FIRMA (Section \ref{subsec:detectionDEG}; Figure \ref{fig:sim10kTStats}).
This observation also is in keeping with the positive bias for BMEA $B$-statistics in Figure \ref{fig:sim10kBStats}.\\
\\
For simulations with varying proportions of Exon 3 (Patterns 6 and 7), fitted values for Exon 3 were spread across overlapping ranges, despite the different true values (Figure \ref{subfig:Sim10Pat6} and Figure \ref{subfig:Sim10Pat7}).
The IQR seen in boxplots for Pattern 7 was slightly lower ($0.22 < \phi_{hj} < 0.28$) than Pattern 6 ($0.32 < \phi_{hj} < 0.40$), however the near overlap of the IQR will likely impact the accuracy of estimation of $\phi_{hj}$.
The impact on detection of AS events would be expected to remain minimal, however.\\

\begin{figure}[p]
  \centering
  \includegraphics[width=0.9\textwidth]{Appendix/phiFitted1to5}
  \caption[Splicing patterns 1 to 5. Boxplot of fitted values for $\phi_{hj}$.]{
       Splicing patterns 1 to 5. 
       Boxplot of fitted values for $\phi_{hj}$ compared to simulated values for each exon within each splicing pattern. 
	   Posterior medians were used as point estimates for fitted values.
	   Exons removed during $Z$-score filtering are shown as being given the value $\phi_{hj}=0$.
	   Outlier points, based on the IQR, are shown as individual dots.
	   All splicing patterns contain transcripts of the same length in both simulated cell-types (Figure \ref{fig:Sim10Patterns}).}
	   \label{fig:phiFitted1to5}
\end{figure}
    
\begin{figure}[p]
  \centering
  \includegraphics[width=0.9\textwidth]{Appendix/phiFitted6to10}
  \caption[Splicing patterns 6 to 10. Boxplot of fitted values for $\phi_{hj}$.]{
      Splicing patterns 6 to 10.
      Boxplot of fitted values for $\phi_{hj}$ compared to simulated values for each exon within each splicing pattern. 
	  Posterior medians were used as point estimates for fitted values.
	  Exons removed during $Z$-score filtering are shown as being given the value $\phi_{hj}=0$.
	  Outlier points, based on the IQR, are shown as individual dots.
	  Cell-type B was simulated with a shorter transcript in all splicing patterns (Figure \ref{fig:Sim10Patterns}).} 
	  \label{fig:phiFitted6to10}
\end{figure}


\clearpage
\subsubsection*{Fold-Change for Genes and Exons}

In addition to the simulated model parameters, the recovery of accurate fold-change estimates was also checked both for logFC ($\Delta\mu$) and for changes in exon inclusion proportions ($\Delta\log\phi_j$).
The values for the changes in expression-level are shown using posterior means as point estimates (Figure \ref{fig:logFcFitted}), with data being transformed to the $\log_2$ scale for easier axis labelling.
The difference between the BMEA approaches was minimal with correlations of $\rho_{BMEA} = 0.847$ and $\rho_{BMEA-Z} = 0.844$.
For simulations with non-zero fold-change, the Bias in estimates of $\vert\Delta\mu\vert$ were $-0.22$ and $-0.18$ for the BMEA and BMEA-Z models respectively revealing a tendency to underestimate the scale of any fold change under both approaches.\\

\begin{figure}[!b]
	\centering
	\includegraphics[width=\linewidth]{Appendix/logFcFitted}
	\caption[Estimates for fold-change for each of the simulated values.]{Estimates for fold-change for each of the simulated values.
	Posterior means were used as point estimates for each simulated gene.
	Values are shown on the $\log_2$ scale for easier interpretation and axis-labelling.} 
	\label{fig:logFcFitted}
\end{figure}

Assessing changes in exon proportions is a less common usage of the term logFC, and in this context the term $\phi$-logFC is sometimes used interchangeably with the term $\Delta\log\phi_j$, and refers to the change in log-transformed proportions.
Posterior medians were taken as point estimates for this comparison.
For exons which were not simulated with any difference in proportions, the fitted values for $\phi$-logFC were clearly centred around the value zero (Figure \ref{fig:phiLfcFitted}).\\
\\
For exon 3 in pattern 2, and the first four exons in pattern 3, which were simulated as missing in all samples, the values for $\phi$-logFC were again centred around zero.
Direct comparison between models is difficult for these particular exons, as the majority were removed during the $Z$-filtering steps for the BMEA-Z model, thus the numbers of points making up these boxplots are strongly unbalanced between the two BMEA approaches.
However, for the unfiltered approach, values clearly were centred around zero, but with a more broad distribution than for those exons present in all samples, showing an increased possibility of Type I errors under this model.\\
\\
For patterns 6 and 7, in which Exon 3 was simulated as being present in differing proportions, the simulated values were effectively given 2-fold and 3-fold change in proportions respectively.
For the remaining exons and patterns, the exon was simulated as simply absent or present in all samples, and as such, the true numeric values for $\Delta\log\phi_j$ were either $\pm \infty$, which simply cannot be recovered under the model specification, with $\phi_{hj} \sim \mathcal{U}(0, 1)$ explicitly not permitting the distributional boundary points during the MCMC process.
However, these values in general showed a great deal of variability as may be expected when true value lies outside the range of the prior, but were strongly consistent with the underlying simulated values.

\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{Appendix/phiLfcFitted}
	\caption[Estimates for fold-change for each exon ($\Delta\log\phi_j$) within the simulated values.]{Estimates for fold-change for each exon ($\Delta\log\phi_j$) within the simulated values.
	Posterior medians were used as point estimates for each exon.
	Values are shown on the $\log_2$ scale for easier interpretation.
	Outliers relative to the IQR are shown as individual points.}
	\label{fig:phiLfcFitted} 
\end{figure}


\section{Technical Performance}\label{sec:TechPerformance}

\subsection{Convergence of Parameters}\label{subsec:convergence}

As well as the effectiveness of recovery for the supplied parameters, the $\hat{r}$ statistic was used to assess convergence for each parameter\spCite{GelmanBayesian2004} described in Section \ref{subsec:FittingSims} (Figure \ref{fig:convergenceStats}).
Good rates of convergence were noted across all parameters with the majority of  $\hat{r}$ values being very close to 1, as would be expected when the independent chains have converged.\\
\\
The same statistics for the individual posterior distributions of the values $\phi_{hj}$ also showed relatively good rates of convergence(Figure \ref{fig:convergencePhi}).
However, exons which were simulated as missing were clearly unable to yield estimates which converge as strongly as for the constitutive exons, indicating longer MCMC run times may be beneficial for candidate AS exons.\\

\begin{figure}[!b]
	\centering
	\includegraphics[width=0.95\linewidth]{Appendix/convergence}
	\caption[Boxplots of the convergence statistic ($\hat{r}$) for key model parameters.]{Boxplots of the convergence statistic ($\hat{r}$) for key model parameters.
	Outliers beyond the limits of the $y$-axis have been omitted.
	Values approaching $\hat{r}=1$ indicate that the model has converged for the given parameter.}
	\label{fig:convergenceStats}
\end{figure}

\begin{figure}[p]
	\centering
	\includegraphics[width=\linewidth]{Appendix/convergencePhi}
	\caption[Boxplots of the convergence statistic ($\hat{r}$) for the values $\phi_{hj}$, broken down by splicing pattern.]{Boxplots of the convergence statistic ($\hat{r}$) for the values $\phi_{hj}$, broken down by splicing pattern.
	Outliers have been omitted. 
	Values approaching $\hat{r}=1$ indicate that the model has converged for the given parameter.}
	\label{fig:convergencePhi}
\end{figure}


\subsection{Computational Time}\label{subsec:compTime}

The package is designed to run genes in batches, with each batch of genes executed on parallel ``nodes" using the package \textit{snow}\spCite{Snow2009}.
As the entire process can take a number of days using a quad-core workstation, values were written to disk in chunks of 16 genes, allowing for minimal data and time loss in the event of a power failure, or other system failure.
Each node writes to a separate folder during model fitting, allowing theoretical execution across multiple machines, although this is untested as it was not required for any dataset under investigation here.
After process completion, the files across the separate nodes are merged into a single set of .CEL files containing all required data.
No optimisation was performed with regard to memory consumption, however the process was able to run to completion on a Windows quad-core workstation within 5 days, for the set of 18 arrays in the \Treg dataset.
Subsequent testing of the \Treg dataset using a multi-threaded Linux server running 20 nodes completed in 17 hours.\\
\\
For the simulated data in Chapter \ref{chap:BuildingR}, genes were fit using a quad-core workstation, with genes being fitted in batches of 4 using 1 gene/node.
The $Z$-score filtered approach was generally about 10\% faster for each batch of 4, with the overall completion time being somewhat comparable between the two approaches.
The overall difference was primarily due to a small number of data points for which the computational time was unexpectedly long (Figure \ref{fig:compTimes}).
The reasons for this were not investigated, but it was assumed that the MCMC process may have ventured into areas for which parameter updating became time-consuming.\\

\begin{figure}[p]
	\centering
	\includegraphics[width=0.8\linewidth]{Appendix/compTimes}
	\caption[Boxplots of time taken for each batch of 4 simulated genes.]{Boxplots of time taken for each batch of 4 simulated genes for the BMEA model and for the BMEA model incorporating $Z$-score filtering, using the simulations from Chapter \ref{chap:BuildingR}.}
	\label{fig:compTimes}
\end{figure}

\section{Discussion of Package Performance}

For recovery of the true values used in the simulation (Section \ref{sec:simDataPack}), several noteworthy behaviours were observed.
The positive bias for fitted values of $\mu$ (Figure \ref{fig:muFitted}) and the negative bias for fitted values of $\phi$ (Figures \ref{fig:phiFitted1to5} and \ref{fig:phiFitted6to10}) are likely two sides of the same coin.
The small numbers of probes per exon, simulated to replicate Exon Array design, leave the posterior distributions for $\phi_{hj}$ as heavily influenced by the prior, with the bias towards the centre of the $\mathcal{U}(0, 1)$ prior being observed here.
In order to compensate for this downwards bias in these values, posterior values for $\mu_h$ were generally forced upwards.
The impact of the prior on this relatively small ($I_1 = I_2 = 4$) dataset also leaves the prior with a relatively strong impact on the posterior distributions.\\
\\
The positive bias noted for $\sigma_{\mu}$ (Figure \ref{fig:sigmaFitted}) were also likely to be affected by the same phenomenon with only 8 values effectively being available for estimation of this parameter in this simulated dataset.
This observation also appeared to be consistent across the range of expression levels.
However, the negative bias for both $\sigma_p$ and $\sigma_S$ appeared to be more associated with data from the lower end of the range of expression values indicating that the model may struggle to fit parameters accurately at this end of the range, as was also noted for the parameter $\mu_h$.
For these simulated genes, the level of background signal would likely be comparable to, or greater than, the true signal, further complicating the parameter fitting process.
This simply reinforces the intuitive idea that higher confidence candidates for experimental follow-up will be found when the signal component of the data strongly outweighs the background noise.\\
\\
In terms of convergence between MCMC chains, both BMEA approaches were also comparable, with BMEA-Z showing slightly higher rates of convergence.
The relatively poor rates of convergence for exons simulated as completely missing or absent (i.e. AS exons; Figure \ref{fig:convergencePhi}) still showed an acceptable rate of convergence for most simulated genes with $>60$\% of AS exons obtaining $\hat{r}$ values $<1.1$.
The overall quality of the results for AS detection revealed that this observation brought minimal detrimental behaviours to the approach.
Given available computational resources, more chains or a larger number of iterations may be preferable.\\