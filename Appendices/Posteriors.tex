% Appendix 
\chapter{MCMC Sampling Procedures} \label{app:Posterior} 

\fancyhead{}
\fancyhead[RO,LE]{Appendix \ref{app:Posterior} \emph{Derived Posterior Distributions}}
\fancyfoot{}
\fancyfoot[LE,RO]{\thepage}

\section{True Signal Component}\label{sec:Appendix:S}

Under BMEA the background signal $B$ and true signal $S$ are considered as independent.
In reality, given the nature of competitive binding, this assumption may be violated to some extent.
However, in terms of the modelling parameters this assumption can be considered to hold.
Thus, the equation
\begin{equation}\label{eq:jointProb}
h(PM) = h(B, S) = f(B).g(S)
\end{equation}
is the basis for model specification. \\

Given the specification of $B_{hijk}$ in Equation \ref{subeq:logBNorm}, and temporarily ignoring the truncation point,  we can see that the background signal for each probe can be modelled using the log-normal probability density function
\begin{equation}\label{eq:pdfB}
f(B_{hijk}) = \frac{1}{B_{hijk} \delta_{hijk} \sqrt{2\pi}} \exp \left[ - \frac{ \left( \log B_{hijk} - \lambda_{hijk}\right)^2 }{2 \delta_{hijk}^2}\right].
\end{equation}

Using the same notation as \ref{sec:completeBMEA}, the subscripts $h$, $i$, $j$ \& $k$ respectively denote the cell-type, chip, exon and probe.
The values $\lambda_{hijk}$ \& $\delta_{hijk}$ are assigned to each probe before BMEA model fitting, and are based on the probe sequence, as described in Section \ref{sec:BGPriors}.\\

%\begin{landscape}
\subsection{Uniform Model}
Noting that $B_{hijk} = PM_{hijk} - S_{hijk}$, Equation \ref{eq:pdfB} can be equivalently expressed as

\begin{align}\label{eq:pdfB2}
	\begin{split}
		f(B_{hijk}) &= f(PM_{hijk} - S_{hijk})  \\
		  &=	\frac{1}{ (PM_{hijk} - S_{hijk}) \delta_{hijk} \sqrt{2\pi}} 
			\exp \left[ - \frac{ ( \log (PM_{hijk} - S_{hijk}) - \lambda_{hijk} )^2}{2 \delta_{hijk}^2} \right].
	\end{split}
\end{align}


Similarly, as specified in Equation \ref{subeq:logSNorm}, the true signal term $S_{hijk}$ will have the density function
\begin{equation}
g(S_{hijk}) = \frac{1}{S_{hijk} \sigma_S \sqrt{2\pi}} \exp \left[- \frac{(\log S_{hijk} - \eta_{hijk})^2}
	{2 \sigma_S^2} \right]
\end{equation}
where
\begin{align*}
\eta_{hijk} = \log \phi_{hj} + c_{hi} + p_{jk}\,.
\end{align*}

This will give the conditional density function of interest as
\begin{equation}\label{eq:conditionalS}
g(S | PM) = \frac{f(PM -S)g(S)}{\int_0^{PM} f(PM -S)g(S)dS}\,.
\end{equation}
%\end{landscape}

\subsubsection*{MCMC Updating for $S$ Under the Uniform Model} 

Temporarily suppressing subscripts for each given probe $PM_{hijk}$, equation \ref{eq:conditionalS} can be simplified to 
\begin{align}\label{eq:gSgivenPM}
\begin{split}
g(S | PM, \eta, \lambda, \delta, \sigma_S) &\propto f(PM - S)g(S) \\
	&\propto \frac{1}{S(PM-S)} \exp \left[ -\frac{ ( \log(PM - S) - \lambda )^2}{2\delta^2} - \frac{\log S - \eta}{2\sigma_S^2} \right],
\end{split}
\end{align}
and
\begin{align}\label{eq:logSgivenPM}
\log g(S | PM, \eta, \lambda, \delta, \sigma_S) \propto -\log \{S(PM-S)\} -\frac{\{\log (PM - S) - \lambda\}^2}{2\delta^2} - \frac{\log S - \eta}{2 \sigma_S^2}\,.
\end{align}

At iteration '$t: t \geq 1$', $S$ can be updated using a Metropolis-Hastings step.
A new value $S^*$ can be proposed by sampling $B^*$ from the prior, truncated at the observed value $PM$
\begin{equation} \label{eq:sampleB}
\log B \sim \mathcal{N}(\lambda, \delta); 0 \leq B \leq PM\,.
\end{equation}

Then, relying on the property $S^* = PM - B^*$ the value is accepted for $S^t$ with probability $r$ using
\begin{align} \label{eq:updateSUniform}
r = \text{min} \left( \frac{g(S^* | PM, \eta, \lambda, \delta, \sigma_S) }{g(S^{t-1} | PM, \eta, \lambda, \delta, \sigma_S ) }, 1 \right)\,.
\end{align}

%\clearpage
\subsection{Mixture Model}\label{subsec:mixtureS}

Under the mixture model, the complete specification of $S$ requires the 3-component mixture model 
\begin{equation}\label{eq:mixtureModel}
S_{hijk} \sim
	\begin{cases}
		\text{Log-}\mathcal{N}(c_{hi} + p_{jk}, \sigma_S) & \quad \phi_{hj} = 1 \\
		\text{Log-}\mathcal{N}(c_{hi}+ p_{jk}+ \log \phi_{hj}, \sigma_S) & \quad 0 < \phi_{hj} < 1 \\
		0 & \quad \phi_{hj} = 0
	\end{cases}\,.
\end{equation}

Group membership for each probe \& exon can be represented using the indicator variable\\

\begin{align*}
\bm{\xi}_{hj} = (\xi_{hj}^1, \xi_{hj}^2, \xi_{hj}^3) \sim \text{Multinomial}(1, \bm{q}_{hj})
\end{align*}
where 
\begin{align*}
\bm{q}_{hj} = (q_{hj}^1, q_{hj}^2, q_{hj}^3); \quad \displaystyle\sum_{n=1}^3 q_{hj}^n = 1\,.
\end{align*}
For notational convenience, let the vector of known values be represented by
\begin{align*}
\bm{\psi} = (\bm{c}, \bm{\mu}, \sigma_{\mu}, \bm{p}, \sigma_p, \sigma_S, \bm{\lambda}, \bm{\delta})\,,
\end{align*}
where
\begin{align*}
\bm{c} &=(c_{hi}; h= 1,\dots, H; i=1,\dots, I_h) \\
\bm{\mu} &= (\mu_h; h = 1, \dots, H)\\
\bm{p} &= (p_{jk}; j = 1, \ldots, J; k= 1, \dots, K_j)\\
\bm{\lambda} &= (\lambda_{hijk}; h= 1,\dots, H; i=1,\dots, I_h; j = 1, \dots J; k = 1, \dots, K_j)\\
\bm{\delta} &= (\delta_{hijk}; h= 1,\dots, H, i=1,\dots, I_h, j = 1, \dots J, k = 1, \dots, K).\\
\end{align*}

The true signal component of the intensity for each probe will have pdf
\begin{equation}
g(S_{hijk} | \bm{\psi}, \bm{\xi}_{hj}, \phi_{hj}) = 
	\begin{cases}
		g^1(S_{hijk} | \bm{\psi}, \phi_{hj}) & \quad \xi_{hj}^1 = 1 \\
		g^2(S_{hijk} | \bm{\psi}, \phi_{hj}) & \quad \xi_{hj}^2 = 1 \\
		g^3(S_{hijk} | \bm{\psi}, \phi_{hj}) & \quad \xi_{hj}^3 = 1 \\
	\end{cases}
\end{equation}
where
\begin{align*}
g^1(S_{hijk} | \bm{\psi}, \phi_{hj}) &= \frac{1}{S_{hijk}\sigma_S\sqrt{2\pi}} \exp \left[ - \frac{ ( \log S_{hijk} - c_{hi} - p_{jk})^2}{2\sigma_S^2} \right] \\
g^2(S_{hijk} | \bm{\psi}, \phi_{hj}) &= \frac{1}{S_{hijk}\sigma_S\sqrt{2\pi}} \exp \left[ - \frac{ ( \log S_{hijk} - c_{hi} - p_{jk} - \log \phi_{hj})^2}{2\sigma_S^2} \right] \\
g^3(S_{hijk} | \bm{\psi}, \phi_{hj}) &= 
	\begin{cases}
	1 & \quad \text{when} S_{hijk} = 0\\
	0 & \quad \text{elsewhere.}
	\end{cases}\,.
\end{align*}

Noting that only $g^2(S_{hijk} | \bm{\psi}, \phi_{hj})$ is dependent on $\phi_{hj}$, the marginal probability for $g^2(S_{hijk} |\bm{\psi})$ can be obtained by solving
\begin{equation} \label{eq:g2givenPsi}
g^2(S_{hijk} |\bm{\psi}) = \int_0^1\frac{1}{S_{hijk}\sigma_S\sqrt{2\pi}}\exp \left[-\frac{(\log S_{hijk} -c_{hi} - p_{jk} - \log  \phi)^2}{2\sigma_S^2} \right] d\phi \,.\\
\end{equation}

Firstly, let
\begin{align*}
u = \log S_{hijk} - c_{hi} - p_{jk}
\end{align*}
and
\begin{align*}
e^x = \phi \Rightarrow d\phi = e^x dx\,.
\end{align*}
Substituting these into equation \ref{eq:g2givenPsi} gives
\begin{align*}
	g^2(S_{hijk} |\bm{\psi}) &= \int_{-\infty}^0 \frac{1}{S_{hijk}\sigma_S\sqrt{2\pi}}\exp \left[ - \frac{(u-x)^2}{2 \sigma_S^2} \right] e^x dx \\
	 &= \frac{1}{S_{hijk}} \int_{-\infty}^0 \frac{1}{\sigma_S \sqrt{2\pi}} \exp \left[ - \frac{(u-x)^2}{2 \sigma_S^2} + x \right] dx\,.
\end{align*}
Taking the terms in the exponent and completing the square:
\begin{align*}
- \frac{(u-x)^2}{2 \sigma_S^2} + x &= \frac{-u^2 +2ux -x^2 +2\sigma_S^2 x}{2 \sigma_S^2} \\
 &= \frac{-u^2 + 2(u+ \sigma_S^2)x - x^2}{2 \sigma_S^2} \\
 &= \frac{-(u+ \sigma_S^2)^2 + 2(u+ \sigma_S^2)x - x^2 }{2 \sigma_S^2} + \frac{(u+ \sigma_S^2)^2 - u^2}{2 \sigma_S^2} \\
 &= \frac{-(x - (u + \sigma_S^2))^2}{2 \sigma_S^2} + \frac{(u+ \sigma_S^2)^2 - u^2}{2 \sigma_S^2} \\
 &= \frac{-(x - (u + \sigma_S^2))^2}{2 \sigma_S^2} + u + \frac{\sigma_S^2}{2}\,.
\end{align*}

Returning to the main equation again:
\begin{align*}
g^2(S_{hijk} |\bm{\psi}) &=  \frac{1}{S_{hijk}} \int_{-\infty}^0 \frac{1}{\sigma_S \sqrt{2\pi}} \exp \left[ \frac{-(x - (u + \sigma_S^2))^2}{2 \sigma_S^2} + u + \frac{\sigma_S^2}{2} \right] dx \\
&= \frac{1}{S_{hijk}} \exp \left( u + \frac{\sigma_S^2}{2} \right) \int_{-\infty}^0 \frac{1}{\sigma_S \sqrt{2\pi}} \exp \left[ \frac{-(x - (u + \sigma_S^2))^2}{2 \sigma_S^2} \right] dx\,.
\end{align*}
Now let
\begin{align*}
t = \frac{x - (u + \sigma_S^2)}{\sigma_S} \Rightarrow dx = \sigma_S dt\,.
\end{align*}

Substituting $t$ into the equation gives
\begin{align*}
g^2(S_{hijk} |\bm{\psi}) &= \frac{1}{S_{hijk}} \exp \left[ u + \frac{\sigma_S^2}{2} \right] \int_{-\infty}^{- \frac{u + \sigma_S^2}{\sigma_S}} \frac{1}{\sqrt{2\pi}} \exp \left[ -\frac{t^2}{2} \right] dt \\
&= \frac{1}{S_{hijk}} \exp \left[ u + \frac{\sigma_S^2}{2} \right] \Phi \left[ -\frac{u + \sigma_S^2}{\sigma_S} \right]
\end{align*}
where $\Phi(x)$ denotes the standard normal cumulative distribution function. \\

Returning the original parameters to the model and simplifying the $S_{hijk}$ terms
\begin{equation}\label{eq:g2marginal}
g^2(S_{hijk} |\bm{\psi}) = \exp \left[ \frac{\sigma_S^2}{2} - c_{hi} - p_{jk}  \right] \Phi \left( -\frac{\log S_{hijk} - c_{hi} - p_{jk} + \sigma_S^2}{\sigma_S} \right)\,.
\end{equation}
%\begin{landscape}
Thus the full set of marginal probabilities can be written as
\begin{equation} \label{eq:gAllMarginalS}
g(S_{hijk} | \bm{\psi}) = 
	\begin{cases}	
		\begin{cases}
			0 & \text{when} ~ S_{hijk} = 0\\
			\frac{1}{S_{hijk}\sigma_S \sqrt{2\pi}} \exp \left[ -\frac{(\log S_{hijk} - c_{hi} - p_{jk})^2}{2\sigma_S} \right] &\text{elsewhere}
		\end{cases} \\
		\,\\
		\begin{cases}
			0 & \text{when} ~ S_{hijk} = 0\\
			\exp \left[ \frac{\sigma_S^2}{2} -c_{hi} - p_{jk} \right] \Phi \left( -\frac{\log S_{hijk} - c_{hi} - p_{jk} + \sigma_S^2}{\sigma_S} \right) & \text{elsewhere}
		\end{cases} \\
		\,\\
		\begin{cases}
			1 & \text{when} ~ S_{hijk} = 0 \\
			0 & \text{elsewhere.}
		\end{cases} 
	\end{cases}\,.
\end{equation}
Noting in the above that 
\begin{align*}
g^1 (S_{hijk} | \bm{\psi}) \equiv S_{hijk} \sim \log\mathcal{N} (c_{hi} + p_{jk}, \sigma_S^2)\,. \\
\end{align*}

We also know that since $PM = B + S$, where $B$ has the pdf as defined in Equation \ref{eq:pdfB}, for each of the mixture components $n=1, 2, 3$ the pdf will be of the form
\begin{align*}
f^n \left( PM_{hijk} | \bm{\psi} \right) = 
	\int_0^{PM_{hijk}}    f \left( PM_{hijk} - S_{hijk} | \bm{\psi} \right)     g^n \left( S_{hijk} | \bm{\psi}  \right) dS\,.
\end{align*}

This can be expressed more explicitly as:

\begin{landscape}
\begin{equation}\label{eq:posteriorPM}
f (PM_{hijk} | \bm{\psi} ) =
	\begin{cases}
	
		\int_0^{PM_{hijk}} \frac{1}{S (PM_{hijk} - S) \delta_{hijk} \sigma_S 2 \pi}
			\exp \left[ -\frac{ \left( \log (PM_{hijk} - S) - \lambda_{hijk} \right)^2}{2 \delta_{hijk}^2} 
							 - \frac{ \left(\log S - c_{hi} - p_{jk} \right)^2}{2 \sigma_S^2} \right] dS
		& \xi_{hj}^1 = 1\\
		\,\\
		\int_0^{PM_{hijk}} \frac{1}{(PM_{hijk} - S) \delta_{hijk} \sqrt{2 \pi}}
			\exp \left[ \frac{\sigma_S^2}{2} -c_{hi} - p_{jk} - \frac{( \log (PM_{hijk} - S) - \lambda_{hijk} )^2}{2 \delta_{hijk}^2} \right]
			\Phi \left( -\frac{\log S_{hijk} - c_{hi} - p_{jk} + \sigma_S^2}{\sigma_S} \right) dS
		& \xi_{hj}^2 = 1\\
		\,\\
		\frac{1}{PM_{hijk} \delta_{hijk} \sqrt{2 \pi}} 
			\exp \left[ - \frac{( \log (PM_{hijk} - S) - \lambda_{hijk} )^2}{2 \delta_{hijk}^2} \right]
		& \xi_{hj}^3 = 1\\
		
	\end{cases}\,,
\end{equation}

and subsequently
\begin{equation}\label{eq:posteriorSgivenPM}
g( S_{hijk} | PM_{hijk}, \bm{\psi}, \bm{\xi_{hj}} ) =
	\begin{cases}
	
		\begin{cases}
			0 & \text{for} S_{hijk} = 0 \\
			\frac{1}{S_{hijk}(PM_{hijk} - S_{hijk}) \delta_{hijk} \sigma_S 2 \pi}
				\exp  \left[ -\frac{ \left( \log (PM_{hijk} - S) - \lambda_{hijk} \right)^2}{2 \delta_{hijk}^2} 
							 - \frac{ \left(\log S - c_{hi} - p_{jk} \right)^2}{2 \sigma_S^2} \right] 
			& \text{elsewhere}				 
		\end{cases}
		& \xi_{hj}^1 = 1\\
		\,\\
		\begin{cases}
			0 & \text{for} S_{hijk} = 0 \\
			\frac{1}{(PM_{hijk} - S) \delta_{hijk} \sqrt{2 \pi}}
				\exp \left[ \frac{\sigma_S^2}{2} -c_{hi} - p_{jk} - \frac{( \log (PM_{hijk} - S) - \lambda_{hijk} )^2}{2 \delta_{hijk}^2} \right]
				\Phi \left( -\frac{\log S_{hijk} - c_{hi} - p_{jk} + \sigma_S^2}{\sigma_S} \right) 
			& \text{elsewhere}
		\end{cases}
		& \xi_{hj}^2 = 1\\
		\,\\
		\begin{cases}
			1 & \text{for} S_{hijk} = 0 \\
			0 & \text{elsewhere}
		\end{cases}
		& \xi_{hj}^3 = 1\,.\\

	\end{cases}
\end{equation}
\end{landscape}

For the set of probes belonging to condition $h$ and exon $j$ with observed values
\begin{align*}
\bm{PM_{hj}} = \left\lbrace PM_{h1j1}, PM_{h1j2}, \dots, PM_{h1jk}, PM_{h2j1}, \dots, PM_{hijk} \right\rbrace
\end{align*}
and corresponding signal estimates
\begin{align*}
\bm{S_{hj}} = \left\lbrace S_{h1j1}, S_{h1j2}, \dots, S_{h1jk}, S_{h2j1}, \dots, S_{hijk} \right\rbrace,
\end{align*}
assuming independence between the signal estimates, the pdf for $\bm{S_{hj}}$ becomes
\begin{align*}
g(\bm{S_{hj}} | \bm{PM_{hj}, \psi, \xi_{hj}}) = \prod_{i=1}^{I_h} \prod_{k=i}^{K_j} g_{ik}(S_{hijk} | \bm{PM_{hj}, \psi, \xi_{hj}})\,.
\end{align*}

Thus,
\begin{equation}
g(\bm{S_{hj}} | \bm{PM_{hj},\psi, \xi_{hj}} ) = 
	\begin{cases}
		\prod_{i=1}^{I_h} \prod_{k=i}^{K_j} g^1(S_{hijk} | PM_{hijk}, \bm{\psi,\xi_{hj}}) & \xi_{hj}^1 = 1\\
		\,\\
		\prod_{i=1}^{I_h} \prod_{k=i}^{K_j} g^2(S_{hijk} | PM_{hijk}, \bm{\psi,\xi_{hj}}) & \xi_{hj}^2 = 1\\
		\, \\
		\prod_{i=1}^{I_h} \prod_{k=i}^{K_j} g^3(S_{hijk} | PM_{hijk}, \bm{\psi,\xi_{hj}}) & \xi_{hj}^3 = 1\\
	\end{cases}\,.
\end{equation}

\clearpage
\subsubsection*{MCMC Updating for $S$ Under the Mixture Model}\label{subsec:mcmcMixtureS}
The parameter $S$ can then updated at iteration `$t$' by the following Metropolis-Hastings approach:
\begin{enumerate}
	\item Generate proposal values $\bm{S_{hj}^*} | \bm{ PM_{hj}, \xi_{hj}^t, \psi^{t-1} }$
	\item Sample $\bm{S_{hj}^t}$ from ($\bm{S_{hj}^*}, \bm{S_{hj}^{t-1}}$) with probability $r_{hj} = \left\lbrace r_{h1j1}, r_{h1j2}, \dots, r_{h1jk}, r_{h2j1}, \dots, r_{hijk} \right\rbrace$, where
	\begin{align*}
		r_{hijk} = \min \left[ 1, \frac{g(S_{hijk}^* | \bm{ PM_{hj}, \xi_{hj}^t, \psi^{t-1} }) f(S_{hijk}^{t-1} | S_{hijk}^*) }{g(S_{hijk}^{t-1} | \bm{ PM_{hj}, \xi_{hj}^t, \psi^{t-1} }) f(S_{hijk}^* | S_{hijk}^{t-1}) } \right]\,.
	\end{align*}
\end{enumerate}

Generation of proposal values for $\bm{S_{hj}^*} | \bm{ PM_{hj}, \xi_{hj}^t \neq (0,0,1) , \psi^{t-1} }$ can be performed using the prior for $B_{hijk}$ truncated at the observed value $PM_{hijk}$ as for the Uniform model.
It should be noted however, that automatic acceptance of $\bm{ S_{hj}^* }$ will occur when $\bm{ \xi_{hj}^{t-1} } = (0, 0, 1)$ \& $\bm{ \xi_{hj}^t } \neq (0, 0, 1)$.

\clearpage
\section{The Expression Related Terms $c_{hi}, \mu_{h}$ \& $\sigma_\mu$}\label{sec:expEff}

Taking all other terms ($S_{hijk}, p_{jk}, \phi_{hj}, \sigma_S$) as being known, we can see that given
\begin{equation}\label{eq:logSexplicit}
	\log S_{hijk} = c_{hi} + p_{jk} +\log \phi_{hj} + \varepsilon_{hijk}
\end{equation}
for
\begin{align*}
	x_{hijk} &= \log S_{hijk} - p_{jk} -\log \phi_{hj}  \\
	\Rightarrow x_{hijk} &\sim \mathcal{N}(c_{hi}, \sigma_S^2) \\
	\Rightarrow \bar{x}_{hi \cdot \cdot} &\sim \mathcal{N}( c_{hi}, \sigma_{hi}^2)  
\end{align*}
where
\begin{align*}
	\bar{x}_{hi\cdot\cdot} &= \frac{1}{K_h} \sum_{\left\lbrace j \in J:\phi_{hj} \neq 0 \right\rbrace} \sum_{k=1}^{K_j} x_{hijk} \\
	\sigma_{hi}^2 &= \frac{\sigma_S^2}{K_h} \\
	K_h &= \sum_{\left\lbrace j \in J:\phi_{hj} \neq 0 \right\rbrace} K_j\,.
\end{align*}
Noting that under the Uniform model $\phi_{hj} \neq 0$ and thus $K_h = K$.

\subsection{Updating the Chip Effects Term ($c_{hi}$)}\label{subsec:updateC}
For the set of $H$ treatment groups, the posterior for 
\begin{align*}
\bm {\theta} = \left( c_{11}, \dots, c_{1I_1}, c_{21}, \dots, c_{2I_2}, \dots, c_{H1}, \dots, c_{HI_H} \right) 
\end{align*}
can thus be written as
\begin{align*}
p \left( \bm{\theta}, \bm{\mu}, \sigma_{\mu}^2 |\bm{x} \right) \propto 
	p \left(\bm{\mu}, \sigma_{\mu}^2 \right)  
	p \left (\bm{\theta} | \bm{\mu}, \sigma_{\mu}^2 \right)
	p \left( \bm{x}| \bm{\theta} \bm{\mu}, \sigma_{\mu}^2 \right) \,.
\end{align*}

From the above, and noting that $\mu_h \sim \mathcal{U}(0, \log 2^{16})$, the three components can then be defined as:
\begin{align*}
	p \left( \bm{x}| \bm{\theta} \bm{\mu}, \sigma_{\mu}^2 \right) 
		&=\prod_{h=1}^H \prod_{i=1}^{I_h} \mathcal{N} \left( c_{hi}, \sigma_{hi}^2 \right) \\
	p \left (\bm{\theta} | \bm{\mu}, \sigma_{\mu}^2 \right) 
		&= \prod_{h=1}^H \prod_{i=1}^{I_h} \mathcal{N} \left( c_{hi} | \mu_h, \sigma_{\mu}^2 \right) \\
	p \left(\bm{\mu}, \sigma_{\mu}^2 \right) 
		&= p( \bm{\mu} | \sigma_{\mu}^2) p ( \sigma_{\mu}^2) \propto p( \sigma_{\mu}^2)\,.
\end{align*}

The full conditional posterior is thus:
\begin{align*}
	p \left( \bm{\theta}, \bm{\mu}, \sigma_{\mu}^2 |\bm{x} \right) &\propto 
		p( \sigma_{\mu}^2) 
		\prod_{h=1}^H \prod_{i=1}^{I_h} \mathcal{N} \left( c_{hi} | \mu_h, \sigma_{\mu}^2 \right) 
		\prod_{h=1}^H \prod_{i=1}^{I_h} \mathcal{N} \left( c_{hi}, \sigma_{hi}^2 \right) \\
		&= p( \sigma_{\mu}^2) 
			\prod_{h=1}^H \prod_{i=1}^{I_h} 
			\frac{1}{\sqrt{2 \pi \sigma_{\mu}^2}} \exp \left[ - \frac{(c_{hi} - \mu_h )^2}{2\sigma_{\mu}^2} \right]
			\frac{1}{\sqrt{2 \pi \sigma_{hi}^2}} \exp \left[ - \frac{(\bar{x}_{hi\cdot\cdot} - c_{hi})^2}{2 \sigma_{hi}^2} \right]\,.
\end{align*}

As per \citeNP[p.~135]{GelmanBayesian2004}, this simplifies to
\begin{align*}
	c_{hi} | \bm{\mu}, \sigma_{\mu}, \bm{x} \sim \mathcal{N} (\hat{c}_{hi}, V_{hi} )
\end{align*}
where
\begin{align*}
	\hat{c}_{hi} = V_{hi} \left( \frac{\bar{x}_{hi\cdot\cdot}}{\sigma_{hi}^2} + \frac{\mu_h}{\sigma_\mu^2} \right) \text{and} \quad
	V_{hi} = \frac{1}{\frac{1}{\sigma_{hi}^2} + \frac{1}{\sigma_\mu^2}} \,.
\end{align*}

Noting that in our model construction $\sigma_{hi} = \dfrac{\sigma_S}{\sqrt{K_h}}$, and $K_h$ is constant across all arrays in condition $H$, this can be rewritten as
\begin{align*}
	\hat{c}_{hi} = V_h \left( \frac{K_h \bar{x}_{hi\cdot\cdot}}{\sigma_S^2} + \frac{\mu_h}{\sigma_\mu^2} \right) \text{and} \quad
	V_{h} = \frac{1}{\frac{K_h}{\sigma_S^2} + \frac{1}{\sigma_\mu^2}} 
		= \frac{\sigma_\mu^2 \sigma_S^2}{K_h \sigma_\mu^2 + \sigma_S^2}\,.
\end{align*}

Thus for each treatment condition
\begin{equation}
	c_{hi} | \bar{x}_{hi}, \mu_h, \sigma_\mu^2, \sigma_S^2 
	\sim \mathcal{N} \left(V_h \left( \frac{K_h \bar{x}_{hi\cdot\cdot}}{\sigma_S^2} + \frac{\mu_h}{\sigma_\mu^2} \right), V_h \right)
\end{equation}
and using this distribution, the $c_{hi}$ terms can be updated using a Gibbs Sampler.

\subsection{Updating the Condition Specific Expression-Level Terms ($\mu_h$)}\label{subsec:updateMu}
As expressed in \citeNP[p.~136]{GelmanBayesian2004}, the posterior for $\bm{\mu} | \sigma_\mu^2, \bm{x}$ can be written as
\begin{align*}
p(\bm{\mu}| \sigma_\mu^2 ,\bm{x}) \sim \mathcal{N} ( \bm{\hat{\mu}}, \bm{V_\mu} )
\end{align*}
for the vectors $\bm{\mu} = (\mu_1, \mu_2, \dots, \mu_h)$ and $\bm{V_\mu} = (V_{\mu_1}, V_{\mu_2}, \dots, V_{\mu_h})$, with $h =1,\dots,H$,  where
\begin{align*}
\hat{\mu}_h 
	&= V_{\mu_h} \sum_{i=1}^{I_h} \frac{\bar{x}_{hi\cdot\cdot}} {\sigma_{hi}^2 + \sigma_\mu^2} \\
V_{\mu_h}^{-1} 
	&= \sum_{i=1}^{I_h} \frac{1}{\sigma_{hi}^2 + \sigma_\mu}  
	= \frac{I_h}{\sigma_{hi}^2 + \sigma_\mu^2} \\
\sigma_{hi}^2  
	&= \frac{\sigma_S^2}{K_h}\,.
\end{align*}

Noting once again that $\sigma_{hi}^2 = \frac{\sigma_S^2}{K_h}$, the values $\hat{\mu}_h$ \& $V_h$ can thus be simplified to
\begin{align*}
V_{\mu_h} &=  \frac{\sigma_S^2 + K_h\sigma_\mu^2}{I_hK_h} \\
\hat{\mu}_h &= \frac{1}{I_h} \sum_{i=1}^{I_h} \bar{x}_{hi\cdot\cdot} = \bar{x}_{h\cdot\cdot\cdot}
\end{align*}
and 
\begin{equation}
\bm{\mu} |\bm{x}, \sigma_\mu^2, \sigma_S^2 \sim \mathcal{N} ( \bm{\hat{\mu}}, \bm{V_\mu}  )\,.
\end{equation}
The vector $\bm{\mu}$ can be updated using a Gibbs Sampler.

\subsection{Updating the Expression-Level Variance Term $\sigma_\mu$}\label{subsec:updateSigmaMu}
Again as expressed in \citeNP[p.~136]{GelmanBayesian2004}, the posterior for the variance term $\sigma_\mu^2$ is defined by
\begin{align*}
p(\sigma_\mu^2 | \bm{x}, \sigma_S ) 
	&\propto \frac{p(\sigma_\mu^2 | \sigma_S) \prod_{h=1}^H \prod_{i=1}^{I_h} \mathcal{N} (\bar{x}_{hi\cdot\cdot} | \mu_h, \sigma_{hi}^2 + \sigma_\mu^2) }{p(\bm{\mu} | \sigma_\mu, \bm{x}, \sigma_S)} \\
	&\propto \frac{p(\sigma_\mu^2 | \sigma_S) \prod_{h=1}^H \prod_{i=1}^{I_h} \left[ 2\pi(\sigma_{hi}^2 + \sigma_\mu^2) \right]^{-\frac{1}{2}} \exp \left[ -\frac{(\bar{x}_{hi\cdot\cdot} - \mu_h)^2}{2(\sigma_{hi}^2 + \sigma_\mu^2)}  \right] }{\prod_{h=1}^H (2\pi V_{\mu_h})^{-\frac{1}{2}} \exp \left[ -\frac{(\mu - \hat{\mu}_h)^2}{2V_{\mu_h}} \right] }\,.
\end{align*}
Noting that the denominator holds for all values of $\mu$, set $\mu = \hat{\mu}_h$ and simplifying gives
\begin{align*}
p(\sigma_\mu^2 | \bm(x), \sigma_S)
	&\propto p(\sigma_\mu^2) 
			\prod_{h=1}^H \left( \frac{\sigma_S^2}{K_h} + \sigma_\mu^2 \right)^{-\frac{I_h - 1}{2}}
			\prod_{i=1}^{I_h} \exp \left[ -\frac{(\bar{x}_{hi\cdot\cdot} - \mu_h)^2}{2(\sigma_{hi}^2 + \sigma_\mu^2)} \right] \\
	&=  p(\sigma_\mu^2) 
			\prod_{h=1}^H \left( \frac{\sigma_S^2}{K_h} + \sigma_\mu^2 \right)^{-\frac{I_h - 1}{2}}
			\exp \left[ -\frac{1}{2} \sum_{i=1}^{I_h} \frac{(\bar{x}_{hi\cdot\cdot} - \mu_h)^2}{(\sigma_{hi}^2 + \sigma_\mu^2)}  \right]\,.
\end{align*}
With the uniform prior $p(\sigma_\mu^2)$
\begin{align*}
p(\sigma_\mu^2 | \bm{x}, \sigma_S)
	&\propto \prod_{h=1}^H \left( \frac{\sigma_S^2}{K_h} + \sigma_\mu^2\right)^{-\frac{I_h - 1}{2}}
			\exp \left[ -\frac{1}{2} \sum_{i=1}^{I_h} \frac{(\bar{x}_{hi\cdot\cdot} - \mu_h)^2}{(\sigma_{hi}^2 + \sigma_\mu^2)}  \right]
\end{align*}
and
\begin{align*}
\log p(\sigma_\mu^2 | \bm(x), \sigma_S) 
	&\propto \sum_{h=1}^H \left[ -\frac{(I_h - 1) \log (\frac{\sigma_S^2}{K_h} + \sigma_\mu^2)}{2} 
		- \sum_{i=1}^{I_h} \frac{(\bar{x}_{hi} - \mu_h)^2}{2(\sigma_{hi}^2 + \sigma_\mu^2)}  \right] \\
	&= -\frac{1}{2} \sum_{h=1}^H \left[ (I_h -1) \log (\frac{\sigma_S^2}{K_h} + \sigma_\mu^2) +SS_h  \right]\,,
\end{align*}
where 
\begin{align*}
SS_h = \sum_{i=1}^{I_h} \frac{(\bar{x}_{hi\cdot\cdot} - \mu_h)^2}{\sigma_{hi}^2 + \sigma_\mu^2}.
\end{align*}
Thus $\sigma_\mu^2$ can be updated at step $t$ using a Metropolis-Hastings step with the proposal generating distribution for $\sigma_\mu^*$ being a Gaussian distribution centred at $\sigma_\mu^{t-1}$, and truncated at the limits of the Uniform prior, i.e.
\begin{align*}
\sigma_\mu^* \sim \mathcal{N}(\sigma_\mu^{t-1}, \tau_\mu); 0 < \sigma_\mu^* < \max[p(\sigma_\mu^2)]\,.
\end{align*}
The variance of the proposal generating distribution $\tau^2$ must be adapted during the burn-in period of the MCMC process to ensure suitable coverage of the posterior probability space.

\clearpage
\section{The Probe-Level Terms}\label{sec:mcmcProbeLevel}
The probe-level term $p_{jk}$ is defined as being nested within each exon $j$, however as exon $j$ is not explicitly included in any associated hyperparameter, the subscript $j$ is omitted in all derivations below for increased simplicity, leaving $k = 1, 2, \ldots, K$.\\

For the probe-level terms $p_{k}$ and $\sigma_p$, all other terms can be taken as known.
Given the original equation
\begin{align*}
\log S_{hijk} = c_{hi} + p_{k} + \log \phi_{hj} + \varepsilon_{hijk}\,,
\end{align*}
we can redefine our dummy variable to be
\begin{align*}
x_{hijk} & = \log S_{hijk} - c_{hi} - \log \phi_{hj} \\
\Rightarrow x_{hijk} &\sim \mathcal{N} ( p_{k}, \sigma_S^2 ) \\
\Rightarrow \bar{x}_{\cdot\cdot\cdot k} &\sim \mathcal{N} ( p_{k}, \sigma_k^2 )\,,
\end{align*}
where 
\begin{align*}
\bar{x}_{\cdot\cdot\cdot k} &= \frac{1}{I_k} \sum_{h \in H:\phi_{hj} \neq 0} \sum_{i=1}^{I_h} x_{hijk} \\
\sigma_k^2 &= \frac{\sigma_S^2}{I_k} \\
I_k &= \sum_{h \in H:\phi_{hj} \neq 0} I_h\,,
\end{align*}
additionally noting that under the Uniform Model $I_k = I$.

\subsection{Updating the Probe Effects ($p_{k}$)}\label{subsec:updateP}
Following the same principles as Section \ref{subsec:updateC}, and as per \citeNP[pp.~132--136]{GelmanBayesian2004}, given the prior $p_{k} \sim \mathcal{N}(0, \sigma_p^2)$ the posterior distribution for our unknown vector $\bm{\theta} = (p_1, p_2, \dots, p_{k}); k = 1, 2, \dots, K$ can be expressed as
\begin{equation}
p_{k} | \sigma_p, \bm{x} \sim \mathcal{N} (\hat{p}_k, V_k)\,,
\end{equation}
where
\begin{align*}
V_k &= \frac{1}{\frac{1}{\sigma_k^2} + \frac{1}{\sigma_p^2}} \\
	  &= \frac{\sigma_p^2 \sigma_S^2}{I_k\sigma_p^2 + \sigma_S^2}
\end{align*}
and 
\begin{align*}
\hat{p}_k &= \frac{\frac{\bar{x}_{\cdot\cdot\cdot k}}{\sigma_k^2}}{\frac{1}{\sigma_k^2} + \frac{1}{\sigma_p^2}} \\
	&= \frac{I_kV_k\bar{x}_{\cdot\cdot\cdot k}}{\sigma_S^2}\,.
\end{align*}
Thus each value $p_{k} \in \bm{\theta}$ can be updated using a Gibbs Sampler.

\subsection{Updating the Probe-Level Variance ($\sigma_p^2$)}\label{subsec:updateSigmaP}
Firstly, we know that the following equality holds
\begin{align*}
p(\sigma_p^2 | \bm{x}) \propto p(\sigma_p^2)p(\bm{x} | \sigma_p^2)\,.
\end{align*}
We can also see that since
\begin{align*}
\bar{x}_{\cdot\cdot\cdot k} | p_{k} \sim \mathcal{N} (p_{k}, \sigma_k^2)
\end{align*}
and since
\begin{align*}
p_{k} &\sim \mathcal{N}(0, \sigma_p^2) \\
\Rightarrow \bar{x}_{\cdot\cdot\cdot k} | \sigma_p^2 &\sim \mathcal{N} (0, \sigma_k^2 + \sigma_p^2)\,,
\end{align*}
this leads to the posterior
\begin{align*}
p(\sigma_p^2 | \bm{x}) &\propto p(\sigma_p^2) \prod_{k=1}^K \mathcal{N} (0, \sigma_k^2 + \sigma_p^2) \\
	&=  p(\sigma_p^2) \prod_{k=1}^K \frac{1}{\sqrt{2 \pi (\sigma_k^2 + \sigma_p^2)}} 
	\exp \left[-\frac{\bar{x}_{\cdot\cdot\cdot k}^2}{2(\sigma_k^2 + \sigma_p^2)}  \right]\,.
\end{align*}
Assuming a Uniform Prior for $p(\sigma_p^2)$ then gives
\begin{align*}
p(\sigma_p^2 | \bm{x}) \propto \prod_{k=1}^K \frac{1}{\sqrt{\sigma_k^2 + \sigma_p^2}} 
\exp \left[-\frac{\bar{x}_{\cdot\cdot\cdot k}^2}{2(\sigma_k^2 + \sigma_p^2)}  \right]\,.
\end{align*}
Thus the posterior $p(\sigma_p^2 | \bm{x})$ can be updated using a Metropolis-Hastings step with log-posterior
\begin{align*}
\log p(\sigma_p^2 | \bm{x}, \sigma_S^2) 
	&\propto \sum_{k=1}^K \left[ -\frac{\log(\sigma_k^2+\sigma_p^2)}{2} - \frac{\bar{x}_{\cdot\cdot\cdot k}^2}{2(\sigma_k^2 + \sigma_p^2)} \right] \\
	&= -\frac{1}{2} \sum_{k=1}^K \left[ \log(\sigma_k^2+\sigma_p^2) + \frac{\bar{x}_{\cdot\cdot\cdot k}^2}{\sigma_k^2 + \sigma_p^2} \right]\,.
\end{align*}

A proposal value $\sigma_p^*$ can be generated at iteration $t$ using a Gaussian Random Walk centred at $\sigma_p^{t-1}$, truncated at the limits of the Uniform Prior $p(\sigma_p^2)$, .i.e.
\begin{align*}
\sigma_p^* \sim \mathcal{N}(\sigma_p^{t-1}, \tau_p) ; 0 < \sigma_p^* < \max \left[ p(\sigma_p^2) \right]\,.
\end{align*}
The variance ($\tau_p$) of the proposal generating distribution must also be adapted during the burn-in period to ensure proper exploration of the probability space for this term.

\clearpage
\section{The Exon-Level Terms ($\phi_{hj}, \xi_{hj}$ \& $q_{hj}$)}\label{sec:mcmcExonLevel}

\subsection{The Mixture Model}\label{subsec:updateExonMixture}
Under the Mixture Prior for $\phi_{hj}$ as detailed in Equation \ref{eq:mixtureModel} an exon can fall into one of three groups, i.e.
\begin{enumerate}
	\item $\bm{\xi_{hj}} = (1, 0, 0) \Rightarrow \phi_{hj} = 1$
	\item $\bm{\xi_{hj}} = (0, 1, 0) \Rightarrow \phi_{hj} \sim \mathcal{U} (0, 1)$
	\item $\bm{\xi_{hj}} = (0, 0, 1) \Rightarrow \phi_{hj} =0$.
\end{enumerate}
Thus before the term $\phi_{hj}$ can be updated, the indicator variable $\bm{\xi_{hj}}$ and the probabilities of group membership $\bm{q_{hj}} = (q_{hj}^1, q_{hj}^2, q_{hj}^3)$ must be updated.
This can be done in the following steps, for $n=1,2,3$
\begin{enumerate}
	\item Calculate $\bm{\hat{\gamma}_{hj}} = (\hat{\gamma}_{hj}^1, \hat{\gamma}_{hj}^2, \hat{\gamma}_{hj}^3)$ where
	\begin{align*}
		\hat{\gamma}_{hj}^n &= E \left( \xi_{hj}^n | \bm{PM_{hj}}, \bm{q_{hj}}, \bm{\psi} \right) \\
			&= \frac{q_{hj}^n \prod_{i=1}^{I_h} \prod_{k=1}^{K_j} f^n(PM_{hijk} | \bm{\psi}) }{\sum_{n=1}^3 q_{hj}^n \prod_{i=1}^{I_h} \prod_{k=1}^{K_j} f^n(PM_{hijk} | \bm{\psi}) }
	\end{align*}
	\item Sample $\bm{\xi_{hj}^t} \sim$ Multinomial$(1, \bm{\hat{\gamma}_{hj}})$
	\item Sample new values for $\bm{q_{hj}^t} \sim$ Dirichlet $(\bm{\xi_{hj}^t} + 1)$.
\end{enumerate}

The functions $f^n(PM_{hijk} | \bm{\psi})$ are defined in Section \ref{subsec:mixtureS} as Equation \ref{eq:posteriorPM}.

\subsection{Updating the Exon Proportion Term ($\phi_{hj}$)}\label{subsec:updatePhi}
Under the Mixture Model, the value $\phi_{hj}$ can only take the values $0$ or $1$ if $\xi_{hj}^2 = 0$.
However, if $\xi_{hj}^2 = 1$, the updating method for the Mixture Model and the Uniform Model become identical.
In all following equations for this updating step, this will be presented in the context of the Uniform model. \\

From Equation \ref{eq:logSexplicit}, we can see that
\begin{align*}
x_{hijk} &\sim \mathcal{N} \left( \log \phi_{hj}, \sigma_S^2 \right) \\
\bar{x}_{h \cdot j \cdot} &\sim \mathcal{N} \left( \log \phi_{hj}, \sigma_{hj}^2 \right) 
\end{align*}
where
\begin{align*}
x_{hijk} &= \log S_{hijk} - c_{hi} -p_{k} \\
\bar{x}_{hj} &= \frac{1}{I_h} \sum_{i=1}^{I_h} \frac{1}{K_j} \sum_{k=1}^{K_j} x_{hijk} \\
\sigma_{hj}^2 &= \frac{\sigma_S^2}{I_h K_j}\,.
\end{align*}

Thus for $\theta = - \log \phi_{hj} \sim$ Exponential$(1)$,
\begin{align*}
f(\theta | \bar{x}_{hj}) &\propto f(\theta) f(\bar{x}_{hj} | \theta) \\
	&\propto \exp\left( -\theta \right) \exp\left(- \frac{(\bar{x}_{hj} + \theta)^2}{2 \sigma_{hj}^2} \right) \\
	&= \exp \left( \frac{-1}{2 \sigma_{hj}^2} \left[2\sigma_{hj}^2 \theta + (\bar{x}_{hj} + \theta)^2 \right] \right)\,.
\end{align*}

Taking the exponent terms and completing the square
\begin{align*}
2\sigma_{hj}^2 \theta + (\bar{x}_{hj} + \theta)^2
	&= \bar{x}_{hj}^2 +2\bar{x}_{hj}\theta + \theta^2 + 2\sigma_{hj}^2 \theta  \\
	&= \bar{x}_{hj}^2 +2(\bar{x}_{hj} + \sigma_{hj}^2)\theta + \theta^2 
			+ 2\bar{x}_{hj}\sigma_{hj}^2 - 2\bar{x}_{hj}\sigma_{hj}^2 + (\sigma_{hj}^2)^2 - (\sigma_{hj}^2)^2	\\
	&= (\bar{x}_{hj} + \sigma_{hj}^2)^2 +2(\bar{x}_{hj} + \sigma_{hj}^2)\theta + \theta^2 
			 - 2\bar{x}_{hj}\sigma_{hj}^2  - (\sigma_{hj}^2)^2 \\
	&\propto   (\bar{x}_{hj} + \sigma_{hj}^2)^2 +2(\bar{x}_{hj} + \sigma_{hj}^2)\theta + \theta^2  \\
	&=  (\bar{x}_{hj} + \sigma_{hj}^2 + \theta)^2 \\
	&= (\theta - (-\bar{x}_{hj} - \sigma_{hj}^2))^2 \,.
\end{align*}
Returning to the full equation gives
\begin{align*}
f ( \theta | \bar{x}_{hj} ) \propto \exp \left( \frac{-1}{2 \sigma_{hj}^2} (\theta - (-\bar{x}_{hj} - \sigma_{hj}^2))^2   \right)
\end{align*}
which we can recognise as the kernel of a normal distribution.
Thus
\begin{align*}
	\theta | \bar{x}_{hj} \sim \mathcal{N} (- \bar{x}_{hj} - \sigma_{hj}^2, \sigma_{hj}^2).
\end{align*}
Substituting $- \log \phi_{hj}$ for $\theta$, and taking note of the range restriction for $\log \phi_{hj}$ gives
\begin{align*}
	- \log \phi_{hj} | \bar{x}_{hj} \sim \mathcal{N} (- \bar{x}_{hj} - \sigma_{hj}^2, \sigma_{hj}^2); 0 < - \log \phi_{hj} < \infty\,.
\end{align*}
By symmetry, we can see
\begin{align*}
	 \log \phi_{hj} | \bar{x}_{hj} \sim \mathcal{N} ( \bar{x}_{hj} + \sigma_{hj}^2, \sigma_{hj}^2); -\infty < \log \phi_{hj} < 0
\end{align*}
and a Gibbs Sampler can be used to update this term for both the Uniform Model, and the Mixture Model given $\xi^2 = 1$.

\clearpage
\section{Signal Variance}\label{sec:mcmcSigmaS}
Assuming all other terms are known, by Equations \ref{subeq:logSNorm} and \ref{eq:eta} we know that
\begin{align*}
	p( S |\eta, \sigma_S) = \frac{1}{S \sigma_S \sqrt{2 \pi}} \exp
		\left[ - \frac{(\log S - \eta)^2}{2 \sigma_S^2}\right]
\end{align*}
with the prior for $\sigma_S^2$ being defined as the Jeffreys Prior in Equation \ref{eq:Jeffreys}.
Thus, the posterior for $\sigma_S^2 | \bm{S}, \bm{\eta}$ can be seen to be
\begin{equation}\label{eq:posteriorSigmaS}
	p(\sigma_S^2 | \bm{S}, \bm{\eta}) \propto p(\sigma_S^2)
		\prod_{h=1}^H \prod_{i=1}^{I_h} \prod_{\left\lbrace j \in J: \phi_{hj} \neq 0 \right\rbrace} \prod_{k=1}^{K_j}
		\frac{1}{S_{hijk} \sqrt{2 \pi \sigma_S^2}} 
		\exp \left[- \frac{1}{2 \sigma_S^2} (\log S_{hijk} - \eta_{hijk})^2 \right]7..
\end{equation}

Defining the values
\begin{align*}
\nu &= \sum_{h=1}^H I_h \sum_{\left\lbrace j \in J: \phi_{hj} \neq 0 \right\rbrace} K_j \\
s^2 &= \frac{1}{\nu} 
	\sum_{h=1}^H \sum_{i=1}^{I_h} 
		\sum_{\left\lbrace j \in J: \phi_{hj} \neq 0 \right\rbrace} \sum_{k=1}^{K_j} (\log S_{hijk} - \eta_{hijk})^2,
\end{align*}
Equation \ref{eq:posteriorSigmaS} then can be seen to be
\begin{align*}
	p(\sigma_S^2 | \bm{S}, \bm{\eta}) &\propto p(\sigma_S^2) (\sigma_S^2)^{\frac{\nu}{2}}
		\exp \left[ - \frac{\nu s^2}{2 \sigma_S^2} \right] \\
	&= (\sigma_S^2)^{\frac{\nu}{2} -1}
		\exp \left[ - \frac{\nu s^2}{2 \sigma_S^2} \right] \\
\end{align*}
which can be recognised as a Scaled Inverse $\chi^2$ distribution.
Thus
\begin{equation}\label{eq:finalPosteriorSigmaS}
\sigma_S^2 | \bm{S}, \bm{\eta} \sim \text{Scaled Inv-} \chi^2 (\nu, s^2)
\end{equation}
and $\sigma_S^2$ can be updated using a Gibbs Sampler.

\clearpage
\section{Parameter Initialisation}
Before any parameter updating is able to take place, initial values must be generated for each of the above parameters.
Considering the reliance of some parameters on other values, the order of initialisation is also important.
In the following description, the superscript $^0$ indicates the initial value for each parameter.

\subsection{$S_{hijk}$}
For each probe, set the initial signal estimate to be
\begin{equation}
	S_{hijk}^0 = PM_{hijk} - B_{hijk}^0\,.
\end{equation}
The initial values for background signal can be sampled directly from the prior using
\begin{align*}
 	\log B_{hijk}^0 \sim \mathcal{N}( \lambda_{hijk}, \delta_{hijk}); \log B_{hijk} < \log PM_{hijk}\,.
\end{align*}

\subsection{$\sigma_S$}
Take a random sample from a locally Uniform distribution
\begin{equation}
	\sigma_S^0 \sim \mathcal{U}(0, 10)\,.
\end{equation}

\subsection{$c_{hi}$}
For each array $i: 1 \leq i \leq I$, randomly sample an integer $M: 1 \leq M \leq K$ then set
\begin{equation}
	c_{hi}^0 = \log S_{hijM}^0\,.
\end{equation}

\subsection{$\mu_h$}
For each condition $h: 1 \leq h \leq H$ set
\begin{equation}
	\mu_h^0 = \frac{1}{I_h} \sum_{i=1}^{I_h} c_{hi}^0\,.
\end{equation}

\subsection{$\sigma_\mu$}
Set
\begin{equation}
	\sigma_\mu^0 = \sqrt{\frac{1}{I - 1} \sum_{h=1}^H \sum_{i=1}^{I_h} (c_{hi}^0 - \mu_h^0)^2}\,.
\end{equation}

\subsection{$\phi_{hj}$}
For each exon $hj: 1 \leq h \leq H; 1 \leq j \leq J$, take a random sample from the prior
\begin{equation}
	\phi_{hj}^0 \sim \mathcal{U}(0, 1)\,.
\end{equation}
Note that this method was chosen even for the Mixture Model, for simplicity.
The tendency of the model towards convergence was relied upon for resolving any MCMC runs where the true nature of the data was highly dissimilar to these initial values.

\subsection{$p_{k}$}
For each probe $k: 1 \leq k \leq K$ set
\begin{equation}
	p_{k}^0 = \frac{1}{I} \sum_{h=1}^H \sum_{i=1}^{I_h} \log S_{hijk}^0 - c_{hi}^0 - \log \phi_{hj}^0\,.
\end{equation}

\subsection{$\sigma_p$}
Set
\begin{equation}
	\sigma_p^0 = \frac{1}{K-1} \sqrt{\sum_{k=1}^K (p_{k}^0)^2}\,.
\end{equation}

\subsection{$q_{hj}$}
For the Mixture model only, the probabilities of group membership $\bm{q_{hj}^0}$ must also be initialised for each combination of $hj: 1 \leq h \leq H; 1 \leq j \leq J$.
This is done by sampling from the prior
\begin{equation}
	\bm{q_{hj}^0}\sim \text{Dirichlet}(3, \bm{\alpha}) \text{, where } \bm{\alpha} = (1, 1, 1)\,.
\end{equation}

\clearpage
\section{MCMC Process Summary}\label{sec:mcmcSummary}

\subsection{Uniform Model}\label{sec:mcmcSummaryUniform}
After initialisation, the Uniform model was simply run by stepping through each of the parameters using the sampling methods described in Sections \ref{sec:Appendix:S} to \ref{sec:mcmcSigmaS}.
The order of updating is not important.

\subsection{Mixture Model}\label{sec:mcmcSummaryMixture}
The process for the Mixture model requires sampling of group membership at the beginning of each iteration as described in Section \ref{subsec:updateExonMixture}, and as such, the process of updating parameters at iteration $t: t \geq 1$ is:
\begin{enumerate}
	\item Beginning at $h=1; j=1$,calculate $\bm{\hat{\gamma}_{hj}} = (\hat{\gamma}_{hj}^1, \hat{\gamma}_{hj}^2, \hat{\gamma}_{hj}^3 )$ where
	\begin{align*}
		\hat{\gamma}_{hj}^n &= E(\xi_{hj}^n | PM, \bm{q_{hj}^{t-1}}, \bm{\psi^{t-1}}) \\
			&= \frac{q_{hj}^n \prod_{i=1}^{I_h} \prod_{k=1}^{K_j} f^n (PM_{hijk} | \bm{\psi^{t-1}}) }
			{\sum_{n=1}^3 q_{hj}^n \prod_{i=1}^{I_h} \prod_{k=1}^{K_j} f^n (PM_{hijk} | \bm{\psi^{t-1}})}
	\end{align*}
	\item Sample $\bm{\xi_{hj}^t} \sim$ Multinomial$(1, \bm{\hat{\gamma}_{hj}})$
	\item Sample new values for $\bm{q_{hj}^t} \sim$ Dirichlet $(3, \bm{\xi_{hj}^t} + 1)$
	\item Update all values of $(S_{hijk}^t | PM, \bm{\xi_{hj}^t}, \bm{\psi^{t-1}} )$ (for $h =1; j=1$)
	\item Update $(\phi_{hj}^t | S^t, \bm{\xi_{hj}^t}, \bm{\psi^{t-1}})$
	\item Repeat for all values of $(h: 1 < h \leq H)$ \& $(j: 1 < j \leq J)$
	\item Update all remaining values as described in Sections  \ref{sec:Appendix:S} to \ref{sec:mcmcSigmaS}.
\end{enumerate}
