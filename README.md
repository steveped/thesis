# README #

This is the complete working directory for my thesis.

### How do I make comments? ###

* The first step will be to create your own account on bitbucket (this site) and remember your password!!!
* Then use the 'Create branch' command on the menu on the left. This will make a complete copy of the entire repository at the time you made the request, except it will be your exclusive copy (i.e. your branch)
* From there on, you can make comments in the individual chapters, by simply beginning each sentence with the percent symbol. Each sentence you write should begin on a new line.
* If you want to rewrite a sentence, comment out the original using the percent symbol then write yours beneath it

### How do I send them back for acceptance ###

* Once you've made the alterations, you can create a pull request using the menu on the left. This will send the branch back to me and I can incorporate any changes.

### Can I do it offline? ###

* Yes, but you'll need to install the following three things:
1. the typesetting language LaTeX, 
2. the LaTeX editor TeXMaker, and 
3. the version control software Git.

* The first step can be time consuming as it's a big install.
* Git also can be confusing, but there are only three commands you'll really need to use. You'll also have to download the repository to your computer using "*git clone*", but use the tutorial online at bitbucket to find how to do that. Once you've done it the commands are:


1.  git commit -a -m 'Add a description here, like Edited Section 4.1' (*This commits your changes to the main control file on your HDD. The "-a" means add all new changes, and the -m indicates that a message is following.*)

2.  git push -u origin master (*This will upload all of your edited files to your online repository.*)

NB: You'll need your password everytime you upload to the online repository.



Once you're happy with your comments, push that latest committed version to me

