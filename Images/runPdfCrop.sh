#!/bin/sh

# NB This can only be run on Ubuntu. so don't be an idiot & try run it on Windows...

# This job's working directory
WORKDIR=${1}
echo Working directory is ${WORKDIR}
cd ${WORKDIR}
echo Start time is `date`
FILES=$(ls *.pdf)

for f in ${FILES}						
do
    echo "Cropping ${f}"
    pdfcrop ${f} ${f}
done

echo "All files have been cropped"
