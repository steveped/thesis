\contentsline {chapter}{Declaration of Authorship}{iii}{dummy.1}
\vspace {1em}
\contentsline {chapter}{Abstract}{vii}{dummy.2}
\vspace {1em}
\contentsline {chapter}{Acknowledgements}{ix}{dummy.3}
\vspace {1em}
\contentsline {chapter}{Contents}{xi}{dummy.4}
\contentsline {chapter}{List of Figures}{xvii}{dummy.6}
\contentsline {chapter}{List of Tables}{xxi}{dummy.8}
\contentsline {chapter}{Abbreviations}{xxiii}{dummy.10}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.13}
\contentsline {section}{\numberline {1.1}Overview}{2}{section.14}
\contentsline {section}{\numberline {1.2}Microarray Technology}{4}{section.15}
\contentsline {subsection}{\numberline {1.2.1}Biological motivation}{4}{subsection.16}
\contentsline {subsection}{\numberline {1.2.2}3' Microarrays}{6}{subsection.18}
\contentsline {subsection}{\numberline {1.2.3}Whole Transcript Microarrays}{6}{subsection.19}
\contentsline {subsection}{\numberline {1.2.4}Chip Description Files}{9}{subsection.21}
\contentsline {section}{\numberline {1.3}Statistical Approaches}{10}{section.22}
\contentsline {subsection}{\numberline {1.3.1}Classical Statistics}{10}{subsection.23}
\contentsline {subsection}{\numberline {1.3.2}Multiple Testing Considerations}{11}{subsection.26}
\contentsline {subsection}{\numberline {1.3.3}Robust Statistics}{11}{subsection.28}
\contentsline {subsection}{\numberline {1.3.4}Bayesian Statistics}{13}{subsection.33}
\contentsline {section}{\numberline {1.4}Technical Considerations for Microarrays}{14}{section.35}
\contentsline {subsection}{\numberline {1.4.1}Optical Correction}{14}{subsection.36}
\contentsline {subsection}{\numberline {1.4.2}Non-specific Binding}{14}{subsection.37}
\contentsline {subsection}{\numberline {1.4.3}Detection Above Background}{14}{subsection.38}
\contentsline {subsection}{\numberline {1.4.4}Quantile Normalisation}{15}{subsection.39}
\contentsline {section}{\numberline {1.5}Background Correction Methods}{16}{section.40}
\contentsline {subsection}{\numberline {1.5.1}The Additive Background Model}{16}{subsection.41}
\contentsline {subsection}{\numberline {1.5.2}Affymetrix Approaches}{16}{subsection.43}
\contentsline {subsection}{\numberline {1.5.3}RMA and GC-RMA}{17}{subsection.44}
\contentsline {section}{\numberline {1.6}Differential Gene Expression}{19}{section.48}
\contentsline {subsection}{\numberline {1.6.1}Moderated $t$-statistics}{19}{subsection.49}
\contentsline {section}{\numberline {1.7}Exon Array Analysis}{20}{section.50}
\contentsline {subsection}{\numberline {1.7.1}Unique Design Properties of Exon Arrays}{20}{subsection.51}
\contentsline {subsection}{\numberline {1.7.2}The Splicing Index and Related Approaches}{20}{subsection.52}
\contentsline {subsection}{\numberline {1.7.3}FIRMA}{21}{subsection.56}
\contentsline {subsection}{\numberline {1.7.4}MM-BGX}{22}{subsection.59}
\contentsline {section}{\numberline {1.8}Chromatin Precipitation Arrays}{23}{section.60}
\contentsline {subsection}{\numberline {1.8.1}Chromatin Immunoprecipitation (ChIP)}{23}{subsection.61}
\contentsline {subsection}{\numberline {1.8.2}ChIP-Chip Analysis}{23}{subsection.62}
\contentsline {subsection}{\numberline {1.8.3}The MAT Background Correction Model}{24}{subsection.63}
\contentsline {section}{\numberline {1.9}Software}{25}{section.65}
\contentsline {subsection}{\numberline {1.9.1}The R Statistical Software Environment}{25}{subsection.66}
\contentsline {subsection}{\numberline {1.9.2}WinBUGS}{25}{subsection.67}
\contentsline {section}{\numberline {1.10}FOXP3 and Regulatory T Cells}{26}{section.68}
\contentsline {subsection}{\numberline {1.10.1}T Cells and the T-cell Receptor}{26}{subsection.69}
\contentsline {subsection}{\numberline {1.10.2}The Regulatory T cell subset}{27}{subsection.70}
\contentsline {subsection}{\numberline {1.10.3}FOXP3 is the Genetic Master Switch}{28}{subsection.71}
\contentsline {subsection}{\numberline {1.10.4}The iT\textsubscript {reg}\xspace subset}{30}{subsection.73}
\contentsline {subsection}{\numberline {1.10.5}Cell Surface Markers}{30}{subsection.74}
\contentsline {subsection}{\numberline {1.10.6}Cord Blood}{31}{subsection.75}
\contentsline {section}{\numberline {1.11}Existing Data}{32}{section.77}
\contentsline {subsection}{\numberline {1.11.1}Previous Array Data}{32}{subsection.78}
\contentsline {subsection}{\numberline {1.11.2}FOXP3 ChIP-Chip Data}{33}{subsection.79}
\contentsline {section}{\numberline {1.12}Closing Comments}{34}{section.80}
\contentsline {chapter}{\numberline {2}Data Inspection and Preliminary Analysis}{35}{chapter.81}
\contentsline {section}{\numberline {2.1}Introduction}{36}{section.82}
\contentsline {subsection}{\numberline {2.1.1}The Four Cell Types}{36}{subsection.83}
\contentsline {subsection}{\numberline {2.1.2}Cell Preparation}{38}{subsection.85}
\contentsline {subsection}{\numberline {2.1.3}RNA Extraction and Hybridisation}{38}{subsection.86}
\contentsline {section}{\numberline {2.2}Data Pre-Processing}{40}{section.88}
\contentsline {subsection}{\numberline {2.2.1}Background Correction}{40}{subsection.89}
\contentsline {subsection}{\numberline {2.2.2}Quality Assessment}{40}{subsection.90}
\contentsline {subsection}{\numberline {2.2.3}Formation of Synthetic Difference-Chips}{43}{subsection.93}
\contentsline {subsection}{\numberline {2.2.4}Power Calculations}{43}{subsection.94}
\contentsline {section}{\numberline {2.3}Differential Gene Expression}{45}{section.96}
\contentsline {subsection}{\numberline {2.3.1}Linear Model Fitting}{45}{subsection.97}
\contentsline {subsection}{\numberline {2.3.2}Detection of Significant Genes}{47}{subsection.100}
\contentsline {subsection}{\numberline {2.3.3}Group Assignment}{50}{subsection.104}
\contentsline {section}{\numberline {2.4}Downstream Gene-Level Analysis}{57}{section.115}
\contentsline {subsection}{\numberline {2.4.1}FOXP3 Targets}{57}{subsection.116}
\contentsline {subsection}{\numberline {2.4.2}Gene Ontology Analysis}{60}{subsection.121}
\contentsline {section}{\numberline {2.5}Exon-Level Analysis}{67}{section.129}
\contentsline {subsection}{\numberline {2.5.1}CDF Selection and Pre-Processing}{67}{subsection.130}
\contentsline {subsection}{\numberline {2.5.2}FIRMA Analysis}{67}{subsection.131}
\contentsline {section}{\numberline {2.6}Discussion}{69}{section.133}
\contentsline {chapter}{\numberline {3}Development of the BMEA Model}{73}{chapter.135}
\contentsline {section}{\numberline {3.1}Development of A New Analytic Model}{74}{section.136}
\contentsline {subsection}{\numberline {3.1.1}Context and Motivation}{74}{subsection.137}
\contentsline {subsection}{\numberline {3.1.2}The Biological Framework}{75}{subsection.138}
\contentsline {subsection}{\numberline {3.1.3}Modelling Using Exon Proportions}{75}{subsection.139}
\contentsline {section}{\numberline {3.2}Initial Simulations}{77}{section.144}
\contentsline {subsection}{\numberline {3.2.1}A Prior Distribution for $\phi _{ij}$}{77}{subsection.145}
\contentsline {subsection}{\numberline {3.2.2}Splicing Patterns For Simulated Data}{79}{subsection.148}
\contentsline {subsection}{\numberline {3.2.3}Additional Parameters for Initial Simulations}{82}{subsection.151}
\contentsline {subsection}{\numberline {3.2.4}Fitting Initial Simulated Data}{83}{subsection.162}
\contentsline {subsection}{\numberline {3.2.5}Simulation Results}{86}{subsection.165}
\contentsline {subsection}{\numberline {3.2.6}Discussion Of Initial Simulation Results}{89}{subsection.169}
\contentsline {section}{\numberline {3.3}The Complete BMEA Model For True Signal}{91}{section.170}
\contentsline {subsection}{\numberline {3.3.1}The Overall Signal Level}{91}{subsection.175}
\contentsline {subsection}{\numberline {3.3.2}Ranking of Results}{95}{subsection.188}
\contentsline {subsection}{\numberline {3.3.3}Simulated Data for the Complete True Signal Term}{96}{subsection.191}
\contentsline {subsection}{\numberline {3.3.4}Inspection of Results}{97}{subsection.202}
\contentsline {section}{\numberline {3.4}Prior Specification for Background Signal}{103}{section.206}
\contentsline {subsection}{\numberline {3.4.1}Three Approaches for Modelling Background Signal}{103}{subsection.207}
\contentsline {subsection}{\numberline {3.4.2}Using Bins to Define Background Signal}{107}{subsection.211}
\contentsline {section}{\numberline {3.5}Model Summary and Discussion}{111}{section.215}
\contentsline {chapter}{\numberline {4}\texttt {BMEA} Software Implementation and Validation}{113}{chapter.217}
\contentsline {section}{\numberline {4.1}Building an R Package}{114}{section.218}
\contentsline {section}{\numberline {4.2}Simulating Data for Package Testing}{115}{section.219}
\contentsline {subsection}{\numberline {4.2.1}Using Observed Values for the Background Signal}{115}{subsection.220}
\contentsline {subsection}{\numberline {4.2.2}Simulating the Signal Component}{117}{subsection.234}
\contentsline {section}{\numberline {4.3}Analysing Simulated Data Using the Package BMEA}{122}{section.239}
\contentsline {subsection}{\numberline {4.3.1}DABG using Z-Scores}{122}{subsection.240}
\contentsline {subsection}{\numberline {4.3.2}Fitting the Complete Model}{125}{subsection.246}
\contentsline {section}{\numberline {4.4}Results From Analysis of Simulated Data}{126}{section.247}
\contentsline {subsection}{\numberline {4.4.1}Detection of Differentially Expressed Genes}{126}{subsection.248}
\contentsline {subsection}{\numberline {4.4.2}Detection of Alternately Spliced Exons}{132}{subsection.254}
\contentsline {section}{\numberline {4.5}Discussion}{138}{section.260}
\contentsline {chapter}{\numberline {5}Application of BMEA to Experimental Datasets}{141}{chapter.261}
\contentsline {section}{\numberline {5.1}Introduction To Experimental Datasets}{142}{section.262}
\contentsline {section}{\numberline {5.2}Additional Preparations for Experimental Dataset}{143}{section.264}
\contentsline {subsection}{\numberline {5.2.1}Alternate Ranking Methods}{143}{subsection.265}
\contentsline {subsection}{\numberline {5.2.2}Creation Of A Custom CDF}{143}{subsection.266}
\contentsline {section}{\numberline {5.3}Analysis of the Gardina Dataset}{146}{section.268}
\contentsline {subsection}{\numberline {5.3.1}Published Analysis}{146}{subsection.269}
\contentsline {subsection}{\numberline {5.3.2}Analysis Using BMEA}{149}{subsection.271}
\contentsline {subsection}{\numberline {5.3.3}Analysis Using FIRMA}{152}{subsection.274}
\contentsline {subsection}{\numberline {5.3.4}Analysis Using MMBGX}{155}{subsection.277}
\contentsline {subsection}{\numberline {5.3.5}Comparison of All Methods Used for the Gardina Dataset}{156}{subsection.282}
\contentsline {section}{\numberline {5.4}Tissue Mixture Dataset}{160}{section.285}
\contentsline {subsection}{\numberline {5.4.1}Initial Model Fitting}{160}{subsection.286}
\contentsline {subsection}{\numberline {5.4.2}Negative Control Analysis}{160}{subsection.287}
\contentsline {subsection}{\numberline {5.4.3}Differential Gene Expression}{166}{subsection.291}
\contentsline {subsection}{\numberline {5.4.4}Initial Selection of Candidate AS Exons}{170}{subsection.295}
\contentsline {subsection}{\numberline {5.4.5}Confirmation of AS Events Using Linear Regression}{173}{subsection.298}
\contentsline {section}{\numberline {5.5}Discussion}{182}{section.306}
\contentsline {chapter}{\numberline {6}BMEA Analysis of the T\textsubscript {reg}\xspace Dataset}{183}{chapter.307}
\contentsline {section}{\numberline {6.1}Initial BMEA Analysis of the T\textsubscript {reg}\xspace Dataset}{184}{section.308}
\contentsline {subsection}{\numberline {6.1.1}Fitting the Dataset}{184}{subsection.309}
\contentsline {subsection}{\numberline {6.1.2}Performance at the Gene Level}{186}{subsection.311}
\contentsline {subsection}{\numberline {6.1.3}Selection of Candidate AS Exons}{202}{subsection.327}
\contentsline {subsection}{\numberline {6.1.4}Verification of Candidate Exons}{206}{subsection.330}
\contentsline {section}{\numberline {6.2}The Impact of Uneven Hybridisation}{209}{section.332}
\contentsline {subsection}{\numberline {6.2.1}Hybridisation Patterns that Correspond to Sample Groups}{209}{subsection.333}
\contentsline {subsection}{\numberline {6.2.2}Quantile Normalisation for BMEA}{213}{subsection.336}
\contentsline {section}{\numberline {6.3}Final BMEA Analysis of the T\textsubscript {reg}\xspace Dataset}{216}{section.339}
\contentsline {subsection}{\numberline {6.3.1}Detection of AS Events}{216}{subsection.340}
\contentsline {subsection}{\numberline {6.3.2}Biological Implications}{222}{subsection.345}
\contentsline {subsection}{\numberline {6.3.3}Alternate Splicing For \gene {FOXP3}}{224}{subsection.347}
\contentsline {section}{\numberline {6.4}Discussion}{229}{section.351}
\contentsline {chapter}{\numberline {7}Conclusion}{231}{chapter.352}
\contentsline {section}{\numberline {7.1}The Wider Context}{232}{section.353}
\contentsline {section}{\numberline {7.2}Key Findings}{233}{section.354}
\contentsline {subsection}{\numberline {7.2.1}Background Signal}{233}{subsection.355}
\contentsline {subsection}{\numberline {7.2.2}The BMEA Model}{234}{subsection.356}
\contentsline {section}{\numberline {7.3}Future Directions}{238}{section.357}
\contentsline {subsection}{\numberline {7.3.1}The BMEA Model}{238}{subsection.358}
\contentsline {subsection}{\numberline {7.3.2}T Cell Research}{240}{subsection.359}
\vspace {2em}
\contentsline {chapter}{\numberline {A}MCMC Sampling Procedures}{243}{appendix.360}
\contentsline {section}{\numberline {A.1}True Signal Component}{243}{section.361}
\contentsline {subsection}{\numberline {A.1.1}Uniform Model}{244}{subsection.364}
\contentsline {subsection}{\numberline {A.1.2}Mixture Model}{245}{subsection.372}
\contentsline {section}{\numberline {A.2}The Expression Related Terms $c_{hi}, \mu _{h}$ \& $\sigma _\mu $}{252}{section.383}
\contentsline {subsection}{\numberline {A.2.1}Updating the Chip Effects Term ($c_{hi}$)}{252}{subsection.385}
\contentsline {subsection}{\numberline {A.2.2}Updating the Condition Specific Expression-Level Terms ($\mu _h$)}{254}{subsection.387}
\contentsline {subsection}{\numberline {A.2.3}Updating the Expression-Level Variance Term $\sigma _\mu $}{254}{subsection.389}
\contentsline {section}{\numberline {A.3}The Probe-Level Terms}{256}{section.390}
\contentsline {subsection}{\numberline {A.3.1}Updating the Probe Effects ($p_{k}$)}{256}{subsection.391}
\contentsline {subsection}{\numberline {A.3.2}Updating the Probe-Level Variance ($\sigma _p^2$)}{257}{subsection.393}
\contentsline {section}{\numberline {A.4}The Exon-Level Terms ($\phi _{hj}, \xi _{hj}$ \& $q_{hj}$)}{259}{section.394}
\contentsline {subsection}{\numberline {A.4.1}The Mixture Model}{259}{subsection.395}
\contentsline {subsection}{\numberline {A.4.2}Updating the Exon Proportion Term ($\phi _{hj}$)}{259}{subsection.402}
\contentsline {section}{\numberline {A.5}Signal Variance}{262}{section.403}
\contentsline {section}{\numberline {A.6}Parameter Initialisation}{263}{section.406}
\contentsline {subsection}{\numberline {A.6.1}$S_{hijk}$}{263}{subsection.407}
\contentsline {subsection}{\numberline {A.6.2}$\sigma _S$}{263}{subsection.409}
\contentsline {subsection}{\numberline {A.6.3}$c_{hi}$}{263}{subsection.411}
\contentsline {subsection}{\numberline {A.6.4}$\mu _h$}{263}{subsection.413}
\contentsline {subsection}{\numberline {A.6.5}$\sigma _\mu $}{264}{subsection.415}
\contentsline {subsection}{\numberline {A.6.6}$\phi _{hj}$}{264}{subsection.417}
\contentsline {subsection}{\numberline {A.6.7}$p_{k}$}{264}{subsection.419}
\contentsline {subsection}{\numberline {A.6.8}$\sigma _p$}{264}{subsection.421}
\contentsline {subsection}{\numberline {A.6.9}$q_{hj}$}{264}{subsection.423}
\contentsline {section}{\numberline {A.7}MCMC Process Summary}{265}{section.425}
\contentsline {subsection}{\numberline {A.7.1}Uniform Model}{265}{subsection.426}
\contentsline {subsection}{\numberline {A.7.2}Mixture Model}{265}{subsection.427}
\contentsline {chapter}{\numberline {B}BMEA Package Design and Performance}{267}{appendix.435}
\contentsline {section}{\numberline {B.1}Package Design}{267}{section.436}
\contentsline {subsection}{\numberline {B.1.1}The MCMC Process}{267}{subsection.437}
\contentsline {subsection}{\numberline {B.1.2}Aroma Affymetrix Conventions}{267}{subsection.438}
\contentsline {subsection}{\numberline {B.1.3}BMEA Extensions to Aroma Affymetrix Structures}{269}{subsection.440}
\contentsline {section}{\numberline {B.2}Algorithm Performance}{272}{section.442}
\contentsline {subsection}{\numberline {B.2.1}Recovery of Simulation Parameters}{272}{subsection.443}
\contentsline {section}{\numberline {B.3}Technical Performance}{283}{section.451}
\contentsline {subsection}{\numberline {B.3.1}Convergence of Parameters}{283}{subsection.452}
\contentsline {subsection}{\numberline {B.3.2}Computational Time}{285}{subsection.455}
\contentsline {section}{\numberline {B.4}Discussion of Package Performance}{287}{section.457}
\contentsline {chapter}{\numberline {C}Tissue Mixture: Inspection Of Individual Exons}{289}{appendix.458}
\contentsline {section}{\numberline {C.1}Manual Inspection Methods}{289}{section.459}
\contentsline {section}{\numberline {C.2}ENSG00000143514 (TP53BP2)}{291}{section.464}
\contentsline {section}{\numberline {C.3}ENSG00000196914 (ARHGEF12)}{294}{section.468}
\contentsline {section}{\numberline {C.4}ENSG00000085832 (EPS15)}{299}{section.473}
\contentsline {section}{\numberline {C.5}ENSG00000075711 (DLG1)}{303}{section.478}
\contentsline {section}{\numberline {C.6}ENSG00000165995 (CACNB2)}{307}{section.483}
\contentsline {section}{\numberline {C.7}ENSG00000133816 (MICAL2)}{311}{section.488}
\contentsline {section}{\numberline {C.8}ENSG00000149294 (NCAM1)}{315}{section.493}
\contentsline {chapter}{\numberline {D}Primer Design for qPCR}{321}{appendix.498}
\vspace {2em}
\contentsline {chapter}{References}{323}{dummy.500}
